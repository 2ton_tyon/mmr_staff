/**
 * 
 */

$(function(){
	
	$('#priceLabel').text($('#price').val());
	$('#identificationLabel').text($('#identificationNumber').val());
	
	$('#price').change(function() {
	    $('#priceLabel').text($('#price').val())
	});
	
	$('#identificationBtn').click(function() {
	    $('#identificationLabel').text($('#identificationNumber').val());
	    $('#outputIdentification').val($('#identificationNumber').val());
	    $('#disposalNumber').attr('max',$('#identificationNumber').val());
	});
	
	$('#disposalBtn').click(function() {
		$('#disposalMsg').text();
		// ①テキストボックスに入力した都市名を変数に格納
	    var add = $('#disposalNumber').val()
	    if( add <= $('#outputIdentification').val()) {
		    // ②appendで動的にチェックボックスを追加
		    $('.disposalList').append('<div><input type="checkbox" name="disposal" value="'+add+'">：'+add+'</div>');
	    } else {
	    	$('#disposalMsg').text('採番した値よりも小さい値を入力してください。');
	    }
	});
	
	$('#return').click(function(){
		$('input[name="disposal"]:checked').parent().remove();

	});
	
	$('#update').click(function(){
		$('input[name="disposal"]').prop('checked', true);
		return true;
	});
	
	$("#category").change(function() {
	    var extraction_val = $("#category").val();
	    if(extraction_val == "1") {
	        $('#CD').css('display', 'block');
			$("#CD").removeAttr("disabled");
	        $('#DVD').css('display', 'none');
			$("#DVD").attr("disabled", "disabled");
			$('#Blu-ray').css('display', 'none');
			$("#Blu-ray").attr("disabled", "disabled");
	    } else if(extraction_val == "2") {
	        $('#CD').css('display', 'none');
			$("#CD").attr("disabled", "disabled");
	        $('#DVD').css('display', 'block');
			$("#DVD").removeAttr("disabled");
			$('#Blu-ray').css('display', 'none');
			$("#Blu-ray").attr("disabled", "disabled");
	    } else if(extraction_val == "3"){
			$('#CD').css('display', 'none');
			$("#CD").attr("disabled", "disabled");
			$('#DVD').css('display', 'none');
			$("#DVD").attr("disabled", "disabled");
			$('#Blu-ray').css('display', 'block');
			$("#Blu-ray").removeAttr("disabled");
		}
	});
});