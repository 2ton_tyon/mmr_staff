/**
 * 
 */
$(function(){
	
	$('[name="edit"]').click(function() {
	    $(this).parent().parent().find('.editingButton').css('display', 'block');
		$(this).parent().css('display', 'none');
		
		$(this).parent().parent().parent().find('.mgrRadio').css('display', 'block');
		$(this).parent().parent().parent().find('.mgrLabel').css('display', 'none');
		
		$(this).parent().parent().parent().find('.nameInput').css('display', 'block');
		$(this).parent().parent().parent().find('.nameLabel').css('display', 'none');
		
		$(this).parent().parent().parent().find('.idInput').css('display', 'block');
		$(this).parent().parent().parent().find('.idLabel').css('display', 'none');
	});
	
	$('[name="cansel"]').click(function() {
		
	    $(this).parent().parent().find('.edit').css('display', 'block');
		$(this).parent().css('display', 'none');
		
		$(this).parent().parent().parent().find('.mgrRadio').css('display', 'none');
		$(this).parent().parent().parent().find('.mgrLabel').css('display', 'block');
		
		$(this).parent().parent().parent().find('.nameInput').css('display', 'none');
		$(this).parent().parent().parent().find('.nameLabel').css('display', 'block');
		
		$(this).parent().parent().parent().find('.idInput').css('display', 'none');
		$(this).parent().parent().parent().find('.idLabel').css('display', 'block');
	});
	
	$('[name="update"]').click(function() {
		$('#outputId').val($(this).parent().parent().parent().find('[name="id"]').val());
		$('#outputName').val($(this).parent().parent().parent().find('[name="name"]').val());
		$('#outputMgr').val($(this).parent().parent().parent().find('input[name="mgr"]:checked').val());
		$('#output').trigger('submit');
	});
	
	$('[name="add"]').click(function() {
		$('#addId').val($(this).parent().parent().find('[name="id"]').val());
		$('#addName').val($(this).parent().parent().find('[name="name"]').val());
		$('#addMgr').val($(this).parent().parent().find('input[name="mgr"]:checked').val());
		$('#add').trigger('submit');
	});
});