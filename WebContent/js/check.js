/**
 * 
 */

$(function(){
	$('#regist').submit(function() {
		var password = $('#password').val();
        var confirmation = $('#confirmPassword').val();
        
        if($('#password') == ""|| $('#confirmPassword') == ""){
        	alert("すべての項目を入力してください");
			return false;
        }
        
        if (password != confirmation) {
            $('#error').text("パスワードが一致していません。");
            return false;
        } else if ($('#password').val().length < 4) {
        	$('#error').text("パスワードが短すぎます。");
            return false;
        } else {
            this.submit();
        }
	});
});