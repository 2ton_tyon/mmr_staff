/**
 * 
 */

$(function(){
	$('#DVD').css('display', 'none');
	$("#DVD").attr("disabled", "disabled");
	$('#Blu-ray').css('display', 'none');
	$("#Blu-ray").attr("disabled", "disabled");
	
	$('#priceLabel').text($('#price').val());
	$('#identificationLabel').text($('#identificationNumber').val());
	
	$('#price').change(function() {
	    $('#priceLabel').text($('#price').val())
	});
	
	$('#identificationBtn').click(function() {
		if (isNaN($('#identificationNumber').val())) {
		    $('#identificationLabel').text($('#identificationNumber').val());
		    $('#outputIdentification').val($('#identificationNumber').val());
		}
	});
	
	$("#category").change(function() {
	    var extraction_val = $("#category").val();
	    if(extraction_val == "1") {
	        $('#CD').css('display', 'block');
			$("#CD").removeAttr("disabled");
	        $('#DVD').css('display', 'none');
			$("#DVD").attr("disabled", "disabled");
			$('#Blu-ray').css('display', 'none');
			$("#Blu-ray").attr("disabled", "disabled");
	    } else if(extraction_val == "2") {
	        $('#CD').css('display', 'none');
			$("#CD").attr("disabled", "disabled");
	        $('#DVD').css('display', 'block');
			$("#DVD").removeAttr("disabled");
			$('#Blu-ray').css('display', 'none');
			$("#Blu-ray").attr("disabled", "disabled");
	    } else if(extraction_val == "3"){
			$('#CD').css('display', 'none');
			$("#CD").attr("disabled", "disabled");
			$('#DVD').css('display', 'none');
			$("#DVD").attr("disabled", "disabled");
			$('#Blu-ray').css('display', 'block');
			$("#Blu-ray").removeAttr("disabled");
		}
	});
});