<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	 <!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/header.css">
    
    <title>MMRオンラインSTAFF</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<br>
	<h2><label style="margin-left:50px">クーポンマスタメンテナンス</label></h2>
	<br>
	<div class="text-center">
		<h3>
		<c:out value="${msg}"/>
		</h3>
		<c:remove var="msg" scope="session"/>
	</div>
	<div class="text-center">
		<h3 style="color:red;">
		<c:out value="${errorMsg}"/>
		</h3>
		<c:remove var="errorMsg" scope="session"/>
	</div>
	<c:choose>
		<c:when test="${newFlg == true}">
			<c:set var="path" value="AddCoupon"/>
			<c:set var="btn" value="追加"/>
		</c:when>
		<c:otherwise>
			<c:set var="path" value="EditCoupon?id=${coupon.id}" />
			<c:set var="btn" value="登録"/>
		</c:otherwise>
	</c:choose>
	<form action="<c:out value="${path}"/>" method="post">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>クーポン編集</h3>
			</div>
			<ul class="list-group">
				<li class="list-group-item">
					<label>内容 : </label>
					<input type="text" name="content" value="<c:out value="${coupon.content}"/>">
				</li>
				<li class="list-group-item">
					<label>割引率 : </label>
					<input type="number" name="discountRate" value="<c:out value="${coupon.discountRate}"/>">
				</li>
				<li class="list-group-item">
					<label>有効期限</label>
					<input type="date" name="activeFrom" value="<c:out value="${coupon.activeFrom}"/>">
					～
					<input type="date" name="activeUntil" value="<c:out value="${coupon.activeUntil}"/>">
				</li>
			</ul>
		</div>
		<button type="submit" class="center-block btn btn-lg btn-success"><c:out value="${btn}"/></button>
	</form>
	<jsp:include page="footer.jsp" />
</body>
</html>