<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%! String[] colors = {"warning","success","info", "danger"}; %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/header.css">

	<title>MMRオンラインSTAFF</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<% request.setAttribute("color",colors); %>
	<br>
	<div class="text-center text-danger">
	<c:if test="${!empty errorMessage}">
	<c:out value="${sessionScope.errorMessage}" />
	<c:remove scope="session" var="errorMessage" />
	</c:if>
	<c:if test="${!empty errorMsg}">
		<c:forEach var="msg" items="${errorMsg}">
			<c:out value="${msg}" />
		</c:forEach>
	<c:remove scope="session" var="errorMsg" />
	</c:if>
	</div>
	<div class="text-center">
		<ul class="pagination justify-content-center pagination-lg">
			<c:choose>
			<c:when test="${page == 1}">
			<li class="page-item disabled">
				<a class="page-link">&laquo;</a>
			</li>
			</c:when>
			<c:otherwise>
			<li class="page-item">
				<a class="page-link" href="<c:out value="SearchRentalView?page=${page-1}&orderDate=${search.orderDatetime}&returnDatePlan=${search.returnPlanDate}&returnDate=${search.returnDate}&rentalStaff=${search.rentalStaffId}&returnStaff=${search.returnStaffId}&status=${search.statusId}" />">&laquo;</a>
			</li>
			</c:otherwise>
			</c:choose>
			<li class="page-item">
				<a class="page-link" href="<c:out value="SearchRentalView?page=${page}&orderDate=${search.orderDatetime}&returnDatePlan=${search.returnPlanDate}&returnDate=${search.returnDate}&rentalStaff=${search.rentalStaffId}&returnStaff=${search.returnStaffId}&status=${search.statusId}" />"><c:out value="${page}"/></a>
			</li>
			<c:choose>
			<c:when test="${(cntRental - (page*10)) > 0}">
			<li class="page-item">
				<a class="page-link" href="<c:out value="SearchRentalView?page=${page+1}&orderDate=${search.orderDatetime}&returnDatePlan=${search.returnPlanDate}&returnDate=${search.returnDate}&rentalStaff=${search.rentalStaffId}&returnStaff=${search.returnStaffId}&status=${search.statusId}" />">&raquo;</a>
			</li>
			</c:when>
			<c:otherwise>
			<li class="page-item disabled">
				<a class="page-link">&raquo;</a>
			</li>
			</c:otherwise>
			</c:choose>
	  	</ul>
	</div>
	<form action="SearchRentalView" method="get">
		<div class="row">
			<div class="col-md-offset-1 col-md-10">
				<table class="table table-hover table-bordered ">
					<tr>
						<th>
						</th>
						<th class="text-center">
							<label>レンタル受付日</label>
							<br>
							<input type="date" name="orderDate" class="form-control">
						</th>
						<th class="text-center">
							<label>返却予定日</label>
							<br>
							<input type="date" name="returnDatePlan" class="form-control">
						</th>
						<th class="text-center">
							<label>返却日</label>
							<br>
							<input type="date" name="returnDate" class="form-control">
						</th>
						<th></th>
						<th class="text-center">
							<label>貸出担当</label>
							<br>
							<select name="rentalStaff" size="1" class="form-control" id="category">
								<option>---</option>
								<c:forEach var="staff" items="${staffList}">
								<option value="${staff.staffId}"><c:out value="${staff.name}"/></option>
								</c:forEach>
							</select>
						</th>
						<th class="text-center">
							<label>返却担当</label>
							<br>
							<select name="returnStaff" size="1" class="form-control" id="category">
								<option>---</option>
								<c:forEach var="staff" items="${staffList}">
								<option value="${staff.staffId}"><c:out value="${staff.name}"/></option>
								</c:forEach>
							</select>
						</th>
						<th class="text-center">
							<label>ステータス</label>
							<br>
							<select name="status" size="1" class="form-control" id="category">
								<option>---</option>
								<c:forEach var="status" items="${statusList}">
								<option value="${status.id}"><c:out value="${status.name}"/></option>
								</c:forEach>
							</select>
						</th>
						<th>
							<br>
							<button type="submit" class="btn btn-default center-block">検索</button>
						</th>
					</tr>
					<tr>
						<th class="text-center">注文受付番号</th>
						<th class="text-center">注文受付時間</th>
						<th class="text-center">返却予定日</th>
						<th class="text-center">返却日</th>
						<th class="text-center">数量</th>
						<th class="text-center">貸出担当</th>
						<th class="text-center">返却担当</th>
						<th class="text-center">ステータス</th>
						<th class="text-center">詳細</th>
					</tr>
					<c:forEach var="rental" items="${rentalList}">
					<tr class="<c:out value="${color[rental.statusId - 1]}"/>">
						<td class="text-center"><c:out value="${rental.rentalNumber}"></c:out></td>
						<td class="text-center"><c:out value="${rental.orderDatetime}"></c:out></td>
						<td class="text-center"><c:out value="${rental.returnPlanDate}"></c:out></td>
						<td class="text-center"><c:out value="${rental.returnDate}"></c:out></td>
						<td class="text-center"><c:out value="${rental.itmeCount}"></c:out></td>
						<td class="text-center"><c:out value="${rental.rentalStaff}"></c:out></td>
						<td class="text-center"><c:out value="${rental.returnStaff}"></c:out></td>
						<td class="text-center"><c:out value="${rental.status}" ></c:out></td>
						<td class="text-center"><button type="button" class="btn btn-default center-block"  onclick="location.href='<c:out value="RentalDetail?rentalNumber=${rental.rentalNumber}"/>'">詳細</button></td>
					</tr>
					</c:forEach>	
				</table>
			</div>
		</div>
	</form>
	<div class="text-center">
		<ul class="pagination justify-content-center pagination-lg">
			<c:choose>
			<c:when test="${page == 1}">
			<li class="page-item disabled">
				<a class="page-link">&laquo;</a>
			</li>
			</c:when>
			<c:otherwise>
			<li class="page-item">
				<a class="page-link" href="<c:out value="SearchRentalView?page=${page-1}&orderDate=${search.orderDatetime}&returnDatePlan=${search.returnPlanDate}&returnDate=${search.returnDate}&rentalStaff=${search.rentalStaffId}&returnStaff=${search.returnStaffId}&status=${search.statusId}" />">&laquo;</a>
			</li>
			</c:otherwise>
			</c:choose>
			<li class="page-item">
				<a class="page-link" href="<c:out value="SearchRentalView?page=${page}&orderDate=${search.orderDatetime}&returnDatePlan=${search.returnPlanDate}&returnDate=${search.returnDate}&rentalStaff=${search.rentalStaffId}&returnStaff=${search.returnStaffId}&status=${search.statusId}" />"><c:out value="${page}"/></a>
			</li>
			<c:choose>
			<c:when test="${(cntRental - (page*10)) > 0}">
			<li class="page-item">
				<a class="page-link" href="<c:out value="SearchRentalView?page=${page+1}&orderDate=${search.orderDatetime}&returnDatePlan=${search.returnPlanDate}&returnDate=${search.returnDate}&rentalStaff=${search.rentalStaffId}&returnStaff=${search.returnStaffId}&status=${search.statusId}" />">&raquo;</a>
			</li>
			</c:when>
			<c:otherwise>
			<li class="page-item disabled">
				<a class="page-link">&raquo;</a>
			</li>
			</c:otherwise>
			</c:choose>
	  	</ul>
	</div>
	<jsp:include page="footer.jsp" />
</body>
</html>