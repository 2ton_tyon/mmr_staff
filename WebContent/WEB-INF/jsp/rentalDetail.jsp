<%@page import="jdk.nashorn.internal.ir.RuntimeNode.Request"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="jp.staff.dto.StatusDTO" %>
<%! String[] colors = {"panel-warning","panel-success","panel-info", "panel-danger"}; %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/header.css">
	<style type="text/css">
    <!--
		.original_box{
		margin-right:50px;
		margin-left:50px;
		}
    //-->
    </style>
	<title>MMRオンラインSTAFF</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<% request.setAttribute("color",colors); %>
	<c:set var="id" value="${rentalDetail.statusId-1}" />
	<c:set var="totalFee" value="0"/>
	<c:forEach var="item" items="${rentalDetail.rentalItemList}">
	<c:set var="totalFee" value="${totalFee+item.price}"/>
	</c:forEach>
	<br>
	<br>
	<div class="text-center text-danger">
		<h2>
			<c:if test="${!empty sessionScope.errorMessage}">
			<c:out value="${sessionScope.errorMessage}"/>
			<c:remove scope="session" var="errorMessage"/>
			</c:if>
		</h2>
	</div>
	<form action="<c:out value="RentalDetail?rentalNumber=${rentalDetail.rentalNumber}"/>" method="post" id="fm">
		<div class="panel original_box <c:out value="${color[id]}"/>" >
			<div class="panel-heading">
				<h3>レンタル詳細</h3>
			</div>
			<div class="panel-body">
				<div class="original_box text-right">
					<h3>
						<label class="text-left">状態</label>
						<label class="text-right"><c:out value="${rentalDetail.statusName}"/></label>
					</h3>
					<hr>
				</div>
				<div class="box2">
					<c:choose>
					<c:when test="${rentalDetail.statusId == 1}">
					<button type="submit" class="btn btn-default btn-lg pull-right" id="rental">貸出</button>
					</c:when>
					<c:when test="${rentalDetail.statusId == 2 or rentalDetail.statusId == 4}">
					<button type="submit" class="btn btn-default btn-lg pull-right" id="return">返却</button>
					</c:when>
					</c:choose>
				</div>
				<br>
				<br>
				<div class="panel panle-default">
					<div class="panel-body text-right">
						<h2><c:out value="合計数量 : ${rentalDetail.totalItem} 合計金額 : ${totalFee}円" /></h2>
					</div>
				</div> 
				<table class="table table-hover table-bordered">
					<tr class="active">
						<th>商品番号</th>
						<th>カテゴリ</th>
						<th>ジャンル</th>
						<th>タイトル</th>
						<th>アーティスト</th>
						<th>値段</th>
						<th>個体識別番号</th>
					</tr>
					<c:forEach var="item" items="${rentalDetail.rentalItemList}">
					<tr>
						<td>
							<input type="hidden" name="itemId" value="${item.itemId}">
							<c:out value="${item.itemId}"/>
						</td>
						<td><c:out value="${item.category}"/></td>
						<td>
						<c:forEach var="genre" items="${item.genreList}">
						<c:out value="${genre} "/>
						</c:forEach>
						</td>
						<td><c:out value="${item.title}"/></td>
						<td><c:out value="${item.artist}"/></td>
						<td><c:out value="${item.price}"/></td>
						<c:choose>
						<c:when test="${rentalDetail.statusId == 1}">
						<td width="50"><input type="text" min="0" max="<c:out value="${item.maxIdentificationNumber}"/>" class="form-control" name="identificationNumber" required></td>
						</c:when>
						<c:otherwise>
						<td><c:out value="${item.identificationNumber}" /></td>
						</c:otherwise>
						</c:choose>
					</tr>
					</c:forEach> 
				</table>
			</div>
		</div>
	</form>
	<script type="text/javascript" src="js/rentalOrReturn.js"></script>
	<jsp:include page="footer.jsp" />
</body>
</html>