<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/header.css">
    
    <title>MMRオンラインSTAFF</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<br>
	<h2><label style="margin-left:100px">クーポンマスタメンテナンス</label></h2>
	<br>
	<div class="text-center">
		<h3>
		<c:out value="${msg}"/>
		</h3>
		<c:remove var="msg" scope="session"/>
	</div>
	<div class="text-center">
		<h3 style="color:red;">
		<c:out value="${errorMsg}"/>
		</h3>
		<c:remove var="errorMsg" scope="session"/>
	</div>
	<div class="row">
		<div class="col-md-offset-10 col-md-1">
			<form action="AddCoupon" method="get">
				<button type="submit" class="btn btn-lg btn-success">追加</button>
			</form>
		</div>
		<div class="col-md-offset-1 col-md-10">
			<table class="table table-hover table-bordered ">
				<tr style="background-color: #F0E68C" class="text-center">
					<td>No</td>
					<td>内容</td>
					<td>有効期限</td>
					<td>配布枚数</td>
					<td>使用枚数</td>
					<td>消化率</td>
					<td>合計割引額</td>
					<td>操作</td>
				</tr>
				<c:forEach var="coupon" items="${couponList}">
				<tr class="text-center">
					<td>
						<label><c:out value="${coupon.id}"/></label>
					<td>
						<label><c:out value="${coupon.content}"/></label>
					</td>
					<td>
						<label><c:out value="${coupon.activeFrom}～${coupon.activeUntil}"/></label>
					</td>
					<td>
						<label><c:out value="${coupon.distributionNumber}"/></label>
					</td>
					<td>
						<label><c:out value="${coupon.usedNumber}"/></label>
					</td>
					<td>
						<c:if test="${coupon.distributionNumber != 0}">
							<label><c:out value="${coupon.usedNumber/coupon.distributionNumber}"/></label>
						</c:if>
						<c:if test="${coupon.distributionNumber == 0}">
							<label><c:out value="0.0"/></label>
						</c:if>
					</td>
					<td>
						<label><c:out value="${coupon.totalDiscountAmount}"/></label>
					</td>
					<td>
						<c:if test="${editArea < coupon.id}" >
							<form action="EditCoupon" method="get">
								<input type="hidden" name="id" value="<c:out value="${coupon.id}"/>">
								<button type="submit" class="btn btn-success">編集</button>
							</form>
							<form action="DeleteCoupon" method="post">
								<input type="hidden" name="id" value="<c:out value="${coupon.id}"/>">
								<button type="submit" class="btn btn-danger">削除</button>
							</form>
						</c:if>
					</td>
				</tr>
				</c:forEach>
			</table>
		</div>
	</div>
	<script type="text/javascript" src="js/staffMaintenance.js"></script>
	<jsp:include page="footer.jsp" />
</body>
</html>