<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="sitename">
	<div class="container">
		<img alt="MMRlogo" src="img/MMR-logo-2.png" class="image-responsive center-block" style="width: 140px; height: 70px" >
	</div>
</div>
<div class="navbar navbar-default" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
			<c:choose>
	     	<c:when test="${sessionScope.auth == 0}">
	     		<li><a href="rental"><i class="fa fa-home"></i>レンタル一覧画面</a></li>
	     		<li><a href="logout"><i class="fa fa-info"></i>ログアウト</a></li>
	     	</c:when>
	     	<c:otherwise>
	     		<li><a href="rental"><i class="fa fa-home"></i>レンタル一覧画面</a></li>
		    	<li><a href="MaintenanceItem"><i class="fa fa-archive" aria-hidden="true"></i>商品マスタメンテナンス</a></li>
		    	<li><a href="StaffMaintenance"><i class="fa fa-address-card" aria-hidden="true"></i>従業員マスタメンテナンス</a></li>
		    	<li><a href="Coupon"><i class="fa fa-money" aria-hidden="true"></i>クーポンマスタメンテナンス</a></li>
		    	<li><a href="DataAnalysis"><i class="fa fa-bar-chart" aria-hidden="true"></i>分析</a></li>
		    	<li><a href="logout"><i class="fa fa-sign-out" aria-hidden="true"></i>ログアウト</a></li>
	     	</c:otherwise>
	     	</c:choose>
	     	</ul>
	   	</div>
	</div>
</div>