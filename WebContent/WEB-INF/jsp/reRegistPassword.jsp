<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<title>MMRオンラインSTAFF-パスワード再設定-</title>
</head>
<body>
	<br>
	<br>
	<div class="row">
		<div class="col-md-offset-4 col-md-4">
			<img alt="MMRlogo" src="img/MMR-logo-2.png" class="image-responsive">
			<div style="color:red">
				<c:out value="${errorMessage}"/>
			</div>
			<h3 class="text-center">パスワードが初期設定になっています。</h3>
			<h3 class="text-center">新しいパスワード設定してください。</h3>
			<form name="loginForm" action="updatePassword" method="post" id="regist">
				<div class="form-group">
					<label>新しいパスワード</label>
					<input type="password" name="password" pattern="^[A-Za-z0-9_]+$" maxlength='41' class="form-control">
				</div>
				<div class="form-group">
					<label>確認用</label>
					<input type="password" name="confirmPassword" pattern="^[A-Za-z0-9_]+$" maxlength='41' class="form-control">
				</div>
				<input class="btn btn-primary center-block" type="submit" value="登録" onclick="return">
				<br>
				<button type="button" class="btn btn-default center-block" onclick="location.href='login'">戻る</button>
			</form>
		</div>
	</div>
	<script type="text/javascript" src="js/check.js"></script>
</body>
</html>