<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
		href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
		href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
	<script
		src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<!-- Chart.js CDN -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
	<link rel="stylesheet" href="css/header.css">
	<title>MMRオンラインSTAFF</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<br>
	<div class="row">
		<div class="col-md-3">
			<h2>
				<label style="margin-left:100px;">クーポン分析画面</label>
			</h2>
		</div>
		<form action="DataAnalysis" method="post">
			<div class="col-md-2" >
				<select name="year" class="form-control">
					<c:forEach var="years" items="${yearList}" varStatus="status">
						<c:if test="${year == years}">
							<option value="${years}" selected><c:out value="${years}"/></option>
						</c:if>
						<c:if test="${year != years}">
							<option value="${years}"><c:out value="${years}"/></option>
						</c:if>
					</c:forEach>
				</select>
			</div>
			<div class="col-md-2">
				<select name="age" class="form-control">
					<c:forEach var="ages" items="${ageList}" varStatus="status">
						<c:if test="${age == status.index+1}">
							<option value="${status.index+1}" selected><c:out value="${ages}"/></option>
						</c:if>
						<c:if test="${age != status.index+1}">
							<option value="${status.index+1}"><c:out value="${ages}"/></option>
						</c:if>
					</c:forEach>
				</select>
			</div>
			<div class="col-md-1">
				<button type="submit" class="btn btn-info">検索</button>
			</div>
		</form>
	</div>
	<br>
	<div class="container" style="width:80%; height:80%">
	    <canvas id="myChart"></canvas>
	</div>
	<br>
	<br>
	<br>
	<script>
	<!--
	var ctx = document.getElementById('myChart').getContext('2d');
	var myChart = new Chart(ctx, {
	  type: 'bar',
	  data: {
	    labels: [<c:forEach var="discountRateList" items="${coupon}" varStatus="status">'<c:out value="${status.index+1}月(${discountRateList.getDiscountRate()}%)" />'<c:if test="${status.index != last}"><c:out value="," /></c:if></c:forEach>],
	    datasets: [{
	      label: '<c:out value="${year}"/>年',
	      borderColor: "rgba(0,0,0,1)",
	      borderWidth: 0.5,
	      data: [<c:forEach var="data" items="${analysis}" varStatus="status"><c:out value="${data.getAmountSold()}"/><c:if test="${status.index != last}"><c:out value=","/></c:if></c:forEach>],
	      backgroundColor: "rgba(40,98,190,1)",
	      options: {
	          showAllTooltips: true}
	    }]
	  }
	});
	-->
	</script>
	<jsp:include page="footer.jsp" />
</body>
</html>