<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/header.css">
    <style type="text/css">
    <!--
		.box{
		margin-right:50px;
		margin-left:50px;
		}
    //-->
    </style>
    <title>MMRオンラインSTAFF</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<c:if test="${!empty sessionScope.errorMsg}">
	<div class="text-center"><h3 style="color:red;"><c:out value="${sessionScope.errorMsg}"/></h3></div>
	<c:remove var="errorMsg" scope="session"/>
	</c:if>
	<div class="text-center">
		<h3>
			新規の商品追加ページです。
			<br>
			イメージの登録は商品の詳細登録後お願いします。
		</h3>
	</div>
	<form action="Add" method="post">
		<div class="panel panel-default box">
			<div class="panel-heading">
				<h2>商品詳細</h2>
			</div>
			<div class="panel-body">
				<div class="panel panel-default">
					<div class="panel-body">
						<ul class="list-group">
							<li class="list-group-item"><c:out value="No.${item.item_id}" /></li>
							<li class="list-group-item">
								<div class="row">
									<div class="col-md-3">
										<label>新旧区分 : </label>
										<select name="newAndOld" class="form-control">
											<option selected value=1>新作</option>
											<option value=2>準新作</option>
											<option value=3>旧作</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-md-5">
										<label>カテゴリ : </label>
										<select name="category" size="1" id="category" class="form-control">
											<c:forEach var="category" items="${categoryList}">
												<c:choose>
													<c:when test="${category.id == item.categoryId}">
														<option value="${category.id}" selected><c:out value="${category.name}" /></option>
													</c:when>
													<c:otherwise>
														<option value="${category.id}"><c:out value="${category.name}" /></option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<label> ジャンル : </label>
										<c:forEach var="category" items="${categoryList}" varStatus="status">
								        	<div id="<c:out value="${category.name}" />" >
								        		<c:forEach var="genre" items="${genreList}">
								        			<c:if test="${category.id == genre.categoryId}">
						        						<input type="checkbox" value="${genre.id}" name="genre"> : <c:out value="${genre.name}" />
								        			</c:if>
								        		</c:forEach>
								        	</div>
										</c:forEach>
									</div>
								</div>
							</li>
							<li class="list-group-item">
								<label>タイトル</label>
								<input type="text" name="title" class="form-control" placeholder="商品名を入力してください" value="<c:out value="${item.itemName}"/>">
							</li>
							<li class="list-group-item">
								<label>監督(アーティスト)</label>
								<input type="text" name="artist" class="form-control" placeholder="監督名(アーティスト名)を入力してください" value="<c:out value="${item.artist}"/>">
							</li>
							<li class="list-group-item">
								<label>料金</label>
								<input name="price" id="price" type="text" class="form-control" placeholder="料金を入力してください" value="<c:out value="${item.price}"/>">
								<span id="priceLabel"></span>円
							</li>
							<li class="list-group-item">
								<label>おすすめの選択 : </label>
								<c:choose>
								<c:when test="${item.recommendedFlag == 0}">
								<input type="radio" name="recommend" value="0" checked="checked">なし
								<input type="radio" name="recommend" value="1">あり
								</c:when>
								<c:otherwise>
								<input type="radio" name="recommend" value="0">なし
								<input type="radio" name="recommend" value="1" checked="checked">あり
								</c:otherwise>
								</c:choose>
							</li>
						</ul>
					</div>
				</div>
				<label>備考欄 : </label>
				<br>
				<textarea name="remarks" rows="10" cols="210"><c:out value="${item.remarks}" /></textarea>
				<hr>
				<label>個体識別番号の入力 : </label>
				<input type="text" id="identificationNumber" min=0 value="<c:out value="${item.maxIdentificationNumber}"/>">
				<input type="hidden" id="outputIdentification" name="identification" value="<c:out value="${item.maxIdentificationNumber}"/>">
				<button type="button" id="identificationBtn" class=" btn btn-lg btn-success">採番</button>
				<br>
				<span id="identificationLabel"></span>まで採番しています
				<hr>
				<button class=" btn btn-lg btn-success" id="update">登録</button>
			</div>
		</div>
	</form>
	<script type="text/javascript" src="js/addItem.js"></script>
	<jsp:include page="footer.jsp" />
</body>
</html>