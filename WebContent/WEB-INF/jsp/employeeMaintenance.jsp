<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	 <!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/header.css">
    
    <title>MMRオンラインSTAFF</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<br>
	<h2><label style="margin-left:100px">従業員マスタメンテナンス</label></h2>
	<br>
	<div class="text-center">
		<h3>
		<c:out value="${msg}"/>
		</h3>
		<c:remove var="msg" scope="session"/>
	</div>
	<div class="text-center">
		<h3 style="color:red;">
		<c:out value="${errorMsg}"/>
		</h3>
		<c:remove var="errorMsg" scope="session"/>
	</div>
	<div class="row">
		<div class="col-md-offset-1 col-md-10">
			<table class="table table-hover table-bordered ">
				<tr style="background-color: #F0E68C" class="text-center">
					<td>従業員ID</td>
					<td>氏名</td>
					<td>管理者権限</td>
					<td>修正</td>
					<td>パスワードリセット</td>
					<td>従業員削除</td>
				</tr>
				<c:forEach var="staff" items="${staffList}">
				<tr class="text-center">
					<td>
						<div class="idInput" style="display:none;">
							<input type="text" name="id" value="<c:out value="${staff.staffId}"/>"> 
						</div>
						<div class="idLabel">
							<label class="idLabel"><c:out value="${staff.staffId}"/></label>
						</div>
					<td>
						<div class="nameInput" style="display:none;">
							<input type="text" name="name" value="<c:out value="${staff.name}"/>"> 
						</div>
						<div class="nameLabel">
							<label class="nameLabel"><c:out value="${staff.name}"/></label>
						</div>
					</td>
					<td>
						<div class="mgrRadio" style="display:none;">
							<c:choose>
							<c:when test="${staff.mgrRights == 1}">
								<input type="radio" name="mgr" value="1" checked ><label> : あり</label>
								<input type="radio" name="mgr" value="0"><label> : なし</label>
							</c:when>
							<c:otherwise>
								<input type="radio" name="mgr" value="1"><label> : あり</label>
								<input type="radio" name="mgr" value="0" checked><label> : なし</label>
							</c:otherwise>
							</c:choose>
						</div>
						<div class="mgrLabel">
							<input type="hidden" value="${staff.mgrRights}" name="defaultMgr">
							<c:choose>
							<c:when test="${staff.mgrRights == 1}">
								<label class="mgrLabel">あり</label>
							</c:when>
							<c:otherwise>
								<label class="mgrLabel">なし</label>
							</c:otherwise>
							</c:choose>
						</div>
					</td>
					<td>
						<c:if test="${myId != staff.staffId}">
							<div class="editingButton" style="display:none;">
								<button type="button" class="btn btn-success" name="update">変更確定</button>
								<button type="button" class="btn btn-default" name="cansel">キャンセル</button>
							</div>
							<div class="edit">
								<button type="button" class="btn btn-info" name="edit">編集</button>
							</div>
						</c:if>
					<td>
						<c:if test="${myId != staff.staffId}">
							<form action="PasswordReset?staffId=<c:out value="${staff.staffId}"/>" method="post">
								<button type="submit" class="btn btn-warning">リセット</button>
							</form>
						</c:if>
					<td>
						<c:if test="${myId != staff.staffId}">
							<form action="DeleteStaff?staffId=<c:out value="${staff.staffId}"/>" method="post">
								<button type="submit" class="btn btn-danger">削除</button>
							</form>
						</c:if>
					</td>
				</tr>
				</c:forEach>
				<tr class="text-center">
					<td>
						<input type="text" name="id" value="<c:out value="${staff.staffId}"/>"> 
					<td>
						<input type="text" name="name" value="<c:out value="${staff.name}"/>"> 
					</td>
					<td>
						<input type="radio" name="mgr" value="1"><label> : あり</label>
						<input type="radio" name="mgr" value="0"><label> : なし</label>
					</td>
					<td>
						<button type="button" class="btn btn-success" name="add">追加</button>
					<td>
					<td>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<form action="UpdateStaff" method="post" id="output" style="display:none;">
		<input type="text" name="id" id="outputId">
		<input type="text" name="name" id="outputName">
		<input type="text" name="mgr" id="outputMgr">
	</form>
	<form action="AddStaff" method="post" id="add" style="display:none;">
		<input type="text" name="id" id="addId">
		<input type="text" name="name" id="addName">
		<input type="text" name="mgr" id="addMgr">
	</form>
	<script type="text/javascript" src="js/staffMaintenance.js"></script>
	<jsp:include page="footer.jsp" />
</body>
</html>