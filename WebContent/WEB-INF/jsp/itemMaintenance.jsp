<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- fontawesome CDN -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap CDN -->
    <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/header.css">
    <title>MMRオンラインSTAFF</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<h2><label style="margin-left:100px">商品マスタメンテナンス</label></h2>
	<div style="margin-left:150px">
	</div>
	<div class="row">
		<div class="row">
			<div class="col-md-offset-1 col-md-6">
				<form class="navbar-form" action="search" method="get">
					<div class="row">
						<div class="col-md-2">
							 <div class="form-group">
						        <select name="category" size="1" class="form-control" id="category">
									<c:forEach var="category" items="${category}">
									<option value="${category}"><c:out value="${category}" /></option>
									</c:forEach>
								</select>
						    </div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
						        <c:forEach var="category" items="${category}" varStatus="status">
								<c:choose>
								<c:when test="${status.index==0}">
								<select name="genre" size="1" class="form-control" id="<c:out value="category${status.index+1}" />">
								<c:forEach var="genre" items="${allGenreList[category]}">
									<option value="${genre}"><c:out value="${genre}" /></option>
								</c:forEach>
								</select>
								</c:when>
								<c:otherwise>
								<select name="genre" size="1" class="form-control" id="<c:out value="category${status.index+1}" />" disabled>
								<c:forEach var="genre" items="${allGenreList[category]}">
									<option value="${genre}"><c:out value="${genre}" /></option>
								</c:forEach>
								</select>
								</c:otherwise>
								</c:choose>
								</c:forEach>
						    </div>
						</div>
						<div class="col-md-4 pull-left">
							<input name="search" type="text" class="form-control">
						</div>
						<div class="col-md-1">
							<button type="submit" class="btn btn-success">検索</button>
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-offset-3 col-md-1">
				<form action="Add" method="get">
					<button type="submit" class="btn btn-lg btn-success">追加</button>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-md-offset-1 col-md-10">
				<table class="table table-hover table-bordered ">
					<tr>
						<th>商品番号</th>
						<th>新旧区分</th>
						<th>カテゴリ</th>
						<th>商品画像</th>
						<th>タイトル</th>
						<th>アーティスト</th>
						<th>値段</th>
						<th>詳細</th>
					</tr>
					<c:forEach var="item" items="${itemList}">
					<tr class="text-center">
						<td><c:out value="${item.item_id}"/></td>
						<td><c:out value="${item.newAndOldName}"/></td>
						<td><c:out value="${item.categoryName}"/></td>
						<td><img style="width: 70px; height: 70px" src="getImage?id=${ item.item_id }" class= "image-responsive"></td>
						<td><c:out value="${item.itemName}"/></td>
						<td><c:out value="${item.artist}" /></td>
						<td><c:out value="${item.price}"/></td>
						<td><button type="button" class="btn btn-default center-block" onclick="location.href='<c:out value="ItemDetail?itemId=${item.item_id}"/>'">詳細</button></td>
					</tr>
					</c:forEach>
				</table>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/select.js"></script>
	<jsp:include page="footer.jsp" />
</body>
</html>