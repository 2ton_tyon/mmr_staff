<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap CDN -->
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet"
	    href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<!-- Bootstrap CDN -->
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<title>MMR ONLINE STAFF</title>
</head>
<body>
	<br>
	<br>
	<div style="text-align:center"><img alt="MMRlogo" src="img/MMR-logo-2.png"></div>
	<div class="row">
		<div class="col-md-offset-4 col-md-4">
			<div style="color:red" class="text-center">
				<c:out value="${errorMessage}"/>
			</div>
			<form name="loginForm" action="login" method="post">
				<div class="form-group">
					<label>従業員ID</label>
					<input type="text" name="id" class="form-control">
				</div>
				<div class="form-group">
					<label>パスワード</label>
					<input type="password" name="password" pattern="^[A-Za-z0-9_]+$" maxlength='41' class="form-control">
				</div>
				<input class="btn btn-primary center-block" type="submit" value="ログイン" >
			</form>
		</div>
	</div>
</body>
</html>