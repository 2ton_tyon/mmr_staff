package jp.staff.staff;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.dao.StaffDAO;
import jp.staff.dto.StaffDTO;

/**
 * Servlet implementation class StaffMaintenanceServlet
 */
@WebServlet("/StaffMaintenance")
public class StaffMaintenanceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		// 管理者権限のある人しか開くことができない
		if (session.getAttribute("id") == null || (Integer)session.getAttribute("auth") != 1) {
			response.sendRedirect("login");
			return;
		}
		
		try (Connection connection = DataSourceManager.getConnection()) {
			StaffDAO staffDAO = new StaffDAO(connection);
			ArrayList<StaffDTO> staffList = staffDAO.selectAll();
			request.setAttribute("myId", session.getAttribute("id"));
			request.setAttribute("staffList", staffList);
			request.getRequestDispatcher("WEB-INF/jsp/employeeMaintenance.jsp").forward(request, response);
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystenError.jsp").forward(request, response);
		}
	}

}
