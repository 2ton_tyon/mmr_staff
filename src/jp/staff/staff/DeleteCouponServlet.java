package jp.staff.staff;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.Validate;
import jp.staff.dao.CouponDAO;
/**
 * Servlet implementation class DeleteCouponServlet
 */
@WebServlet("/DeleteCoupon")
public class DeleteCouponServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteCouponServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null) {
			response.sendRedirect("login");
			return;
		}
		response.sendRedirect("rental");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		if (session.getAttribute("id") == null || (Integer)session.getAttribute("auth") != 1) {
			response.sendRedirect("login");
			return;
		}
		String id = request.getParameter("id");
		if (!Validate.isStrParseInt(id)) {
			response.sendRedirect("rental");
			return;
		}
		try (Connection connection = DataSourceManager.getConnection()) {
			CouponDAO couponDAO2 = new CouponDAO(connection);
			
			connection.setAutoCommit(false);
			// ユーザーが持ってるクーポンを削除する
			// クーポンをクーポンテーブルから削除する
			couponDAO2.deleteHavingCoupon(Integer.parseInt(id));
			int cnt2 = couponDAO2.deleteCoupon(Integer.parseInt(id));
			if (cnt2 == 1) {
				connection.commit();
				session.setAttribute("msg", id + "を削除しました。");
				response.sendRedirect("Coupon");
			} else {
				connection.rollback();
				session.setAttribute("errorMsg", "削除に失敗しました。");
				response.sendRedirect("Coupon");
			}
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
		}
	}

}
