package jp.staff.staff;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.Validate;
import jp.staff.dao.StaffDAO;

/**
 * Servlet implementation class PasswordResetServlet
 */
@WebServlet("/PasswordReset")
public class PasswordResetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		// 管理者権限のある人しか開くことができない
		if (session.getAttribute("id") == null) {
			response.sendRedirect("login");
			return;
		}
		
		if ((Integer)session.getAttribute("auth") == 0) {
			response.sendRedirect("rental");
			return;
		}
		response.sendRedirect("StaffMaintenance");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		// 管理者権限のある人しか開くことができない
		if (session.getAttribute("id") == null || (Integer)session.getAttribute("auth") != 1) {
			response.sendRedirect("login");
			return;
		}
		String id = request.getParameter("staffId");
		if (Validate.isEmpty(id)) {
			response.sendRedirect("rental");
			return;
		}
		if (session.getAttribute("id") == id) {
			//　自分のアカウントを修正することができない
			session.setAttribute("errorMsg", "不正な操作を検知しました。");
			response.sendRedirect("StaffMaintenance");
			return;
		}
		try (Connection connection = DataSourceManager.getConnection()) {
			StaffDAO staffDAO = new StaffDAO(connection);
			int cnt = staffDAO.resetPassword(id);
			if (cnt == 1) {
				session.setAttribute("msg", id + "のパスワードをリセットしました。");
				response.sendRedirect("StaffMaintenance");
			} else {
				session.setAttribute("errorMsg", "パスワードリセットに失敗しました。");
				response.sendRedirect("StaffMaintenance");
			}
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
		}
	}

}
