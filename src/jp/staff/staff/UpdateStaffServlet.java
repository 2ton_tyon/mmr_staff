package jp.staff.staff;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.Validate;
import jp.staff.dao.StaffDAO;
import jp.staff.dto.StaffDTO;

/**
 * Servlet implementation class UpdateStaffServlet
 */
@WebServlet("/UpdateStaff")
public class UpdateStaffServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		// 管理者権限のある人しか開くことができない
		if (session.getAttribute("id") == null) {
			response.sendRedirect("login");
			return;
		}
		
		if ((Integer)session.getAttribute("auth") == 0) {
			response.sendRedirect("rental");
			return;
		}
		response.sendRedirect("StaffMaintenance");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		// 管理者権限のある人しか開くことができない
		if (session.getAttribute("id") == null || (Integer)session.getAttribute("auth") != 1) {
			response.sendRedirect("login");
			return;
		}
		request.setCharacterEncoding("utf-8");
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String mgr = request.getParameter("mgr");
		
		if (Validate.isEmpty(id) || Validate.isEmpty(name) || !Validate.isStrParseInt(mgr)) {
			session.setAttribute("errorMsg", "すべての項目を入力してください");
			response.sendRedirect("StaffMaintenance");
			return;
		}
		if (session.getAttribute("id") == id) {
			//　自分のアカウントを修正することができない
			session.setAttribute("errorMsg", "不正な操作を検知しました。");
			response.sendRedirect("StaffMaintenance");
			return;
		}
		
		try (Connection connection = DataSourceManager.getConnection()) {
			StaffDAO staffDAO = new StaffDAO(connection);
			StaffDTO staffDTO = new StaffDTO();
			staffDTO.setStaffId(id);
			staffDTO.setName(name);
			staffDTO.setMgrRights(Integer.parseInt(mgr));
			int cnt = staffDAO.updateStaff(staffDTO);
			if (cnt == 1) {
				session.setAttribute("msg", id + "を更新しました。");
				response.sendRedirect("StaffMaintenance");
			} else {
				session.setAttribute("errorMsg", "更新に失敗しました。");
				response.sendRedirect("StaffMaintenance");
			}
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystenError.jsp").forward(request, response);
		}
	}

}
