package jp.staff.dto;

import java.util.ArrayList;

public class RentalItemDTO {
	
	private int itemId;
	private String category;
	private ArrayList<String> genreList;
	private String title;
	private String artist;
	private int price;
	private Integer identificationNumber;
	private Integer maxIdentificationNumber;
	
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public ArrayList<String> getGenreList() {
		return genreList;
	}
	public void setGenreList(ArrayList<String> genreList) {
		this.genreList = genreList;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public Integer getIdentificationNumber() {
		return identificationNumber;
	}
	public void setIdentificationNumber(Integer identificationNumber) {
		this.identificationNumber = identificationNumber;
	}
	
	public Integer getMaxIdentificationNumber() {
		return maxIdentificationNumber;
	}
	public void setMaxIdentificationNumber(Integer maxIdentificationNumber) {
		this.maxIdentificationNumber = maxIdentificationNumber;
	}

}
