package jp.staff.dto;

public class RentalDTO {
	private int rentalNumber;
	private String orderDatetime;
	private String returnPlanDate;
	private String returnDate;
	private int itmeCount;
	private String rentalStaffId;
	private String rentalStaff;
	private String returnStaffId;
	private String returnStaff;
	private String statusId;
	private String status;
	public int getRentalNumber() {
		return rentalNumber;
	}
	public void setRentalNumber(int rentalNumber) {
		this.rentalNumber = rentalNumber;
	}
	public String getOrderDatetime() {
		return orderDatetime;
	}
	public void setOrderDatetime(String orderDatetime) {
		this.orderDatetime = orderDatetime;
	}
	public String getReturnPlanDate() {
		return returnPlanDate;
	}
	public void setReturnPlanDate(String returnPlanDate) {
		this.returnPlanDate = returnPlanDate;
	}
	public String getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}
	public int getItmeCount() {
		return itmeCount;
	}
	public void setItmeCount(int itmeCount) {
		this.itmeCount = itmeCount;
	}
	public String getRentalStaffId() {
		return rentalStaffId;
	}
	public void setRentalStaffId(String rentalStaffId) {
		this.rentalStaffId = rentalStaffId;
	}
	public String getRentalStaff() {
		return rentalStaff;
	}
	public void setRentalStaff(String rentalStaff) {
		this.rentalStaff = rentalStaff;
	}
	public String getReturnStaffId() {
		return returnStaffId;
	}
	public void setReturnStaffId(String returnStaffId) {
		this.returnStaffId = returnStaffId;
	}
	public String getReturnStaff() {
		return returnStaff;
	}
	public void setReturnStaff(String returnStaff) {
		this.returnStaff = returnStaff;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}	
	
}
