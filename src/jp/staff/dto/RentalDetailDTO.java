package jp.staff.dto;

import java.util.ArrayList;

public class RentalDetailDTO {
	
	private int rentalNumber;
	private int statusId;
	private String statusName;
	private int totalItem;
	public int getTotalItem() {
		return totalItem;
	}
	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}
	private ArrayList<RentalItemDTO> rentalItemList;
	
	
	public int getRentalNumber() {
		return rentalNumber;
	}
	public void setRentalNumber(int rentalNumber) {
		this.rentalNumber = rentalNumber;
	}
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statudId) {
		this.statusId = statudId;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public ArrayList<RentalItemDTO> getRentalItemList() {
		return rentalItemList;
	}
	public void setRentalItemList(ArrayList<RentalItemDTO> rentalItemList) {
		this.rentalItemList = rentalItemList;
	}

}
