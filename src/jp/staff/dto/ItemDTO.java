package jp.staff.dto;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class ItemDTO {
	private int item_id;
	private String itemName;
	private int categoryId;
	private String categoryName;
	private ArrayList<String> genreName;
	private ArrayList<Integer> genreId;
	private int recommendedFlag;
	private int newAndOldId;
	private String newAndOldName;
	private BufferedImage image;
	private String artist;
	private int price;
	private String remarks;
	private Integer maxIdentificationNumber;
	private ArrayList<Integer> disposalIdentificationNumber;
	
	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public ArrayList<String> getGenreName() {
		return genreName;
	}
	public void setGenreName(ArrayList<String> genreName) {
		this.genreName = genreName;
	}
	public ArrayList<Integer> getGenreId() {
		return genreId;
	}
	public void setGenreId(ArrayList<Integer> genreId) {
		this.genreId = genreId;
	}
	public int getRecommendedFlag() {
		return recommendedFlag;
	}
	public void setRecommendedFlag(int recommendedFlag) {
		this.recommendedFlag = recommendedFlag;
	}
	public int getNewAndOldId() {
		return newAndOldId;
	}
	public void setNewAndOldId(int newAndOldId) {
		this.newAndOldId = newAndOldId;
	}
	public String getNewAndOldName() {
		return newAndOldName;
	}
	public void setNewAndOldName(String newAndOldName) {
		this.newAndOldName = newAndOldName;
	}
	public BufferedImage getImage() {
		return image;
	}
	public void setImage(BufferedImage image) {
		this.image = image;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Integer getMaxIdentificationNumber() {
		return maxIdentificationNumber;
	}
	public void setMaxIdentificationNumber(Integer maxIdentificationNumber) {
		this.maxIdentificationNumber = maxIdentificationNumber;
	}
	public ArrayList<Integer> getDisposalIdentificationNumber() {
		return disposalIdentificationNumber;
	}
	public void setDisposalIdentificationNumber(ArrayList<Integer> disposalIdentificationNumber) {
		this.disposalIdentificationNumber = disposalIdentificationNumber;
	}
}
