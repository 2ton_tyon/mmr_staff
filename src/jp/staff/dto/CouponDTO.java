package jp.staff.dto;

public class CouponDTO {
	
	private int id;
	private String content;
	private String activeFrom;
	private String activeUntil;
	// 割引率
	private int discountRate;
	// 配布枚数
	private int distributionNumber;
	// 使用枚数
	private int usedNumber;
	// 合計割引金額
	private int totalDiscountAmount;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getActiveFrom() {
		return activeFrom;
	}
	public void setActiveFrom(String activeFrom) {
		this.activeFrom = activeFrom;
	}
	public String getActiveUntil() {
		return activeUntil;
	}
	public void setActiveUntil(String activeUntil) {
		this.activeUntil = activeUntil;
	}
	public int getDiscountRate() {
		return discountRate;
	}
	public void setDiscountRate(int discountRate) {
		this.discountRate = discountRate;
	}
	public int getDistributionNumber() {
		return distributionNumber;
	}
	public void setDistributionNumber(int distributionNumber) {
		this.distributionNumber = distributionNumber;
	}
	public int getUsedNumber() {
		return usedNumber;
	}
	public void setUsedNumber(int usedNumber) {
		this.usedNumber = usedNumber;
	}
	public int getTotalDiscountAmount() {
		return totalDiscountAmount;
	}
	public void setTotalDiscountAmount(int totalDiscountAmount) {
		this.totalDiscountAmount = totalDiscountAmount;
	}

}
