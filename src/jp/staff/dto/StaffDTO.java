package jp.staff.dto;

public class StaffDTO {
	public String staffId;
	public String name;
	public int mgrRights;
	public String getStaffId() {
		return staffId;
	}
	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMgrRights() {
		return mgrRights;
	}
	public void setMgrRights(int mgrRights) {
		this.mgrRights = mgrRights;
	}

	

}
