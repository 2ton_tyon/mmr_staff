package jp.staff.dto;

import java.io.InputStream;

public class UploadDto {

	private int id;
	private InputStream image;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the image
	 */
	public InputStream getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(InputStream image) {
		this.image = image;
	}

}
