package jp.staff.coupon;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.dao.CouponDAO;
import jp.staff.dto.CouponDTO;

/**
 * Servlet implementation class CouponMaintenanceServlet
 */
@WebServlet("/Coupon")
public class CouponMaintenanceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null || (Integer) session.getAttribute("auth") != 1) {
			response.sendRedirect("login");
			return;
		}
		
		try (Connection connection = DataSourceManager.getConnection()) {
			CouponDAO couponDAO = new CouponDAO(connection);
			ArrayList<CouponDTO> couponList = couponDAO.selectAll();
			int editArea = couponDAO.selectCouponEditArea();
			request.setAttribute("couponList", couponList);
			request.setAttribute("editArea", editArea);
			request.getRequestDispatcher("WEB-INF/jsp/couponMaintenance.jsp").forward(request, response);
		} catch (SQLException |SQLRuntimeException e) {
			// DB接続エラー
			request.getRequestDispatcher("WEB-INF/jsp/SystenError.jsp").forward(request, response);
		}
	}

}
