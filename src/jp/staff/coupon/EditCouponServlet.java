package jp.staff.coupon;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.Validate;
import jp.staff.dao.CouponDAO;
import jp.staff.dto.CouponDTO;

/**
 * Servlet implementation class EditCouponServlet
 */
@WebServlet("/EditCoupon")
public class EditCouponServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null || (Integer) session.getAttribute("auth") != 1) {
			response.sendRedirect("login");
			return;
		}
		String couponId = request.getParameter("id");
		if (!Validate.isStrParseInt(couponId)) {
			session.setAttribute("errorMsg", "不適切な値が入力されました。");
			response.sendRedirect("Coupon");
			return;
		}
		int id = Integer.parseInt(couponId);
		try (Connection connection = DataSourceManager.getConnection()) {
			CouponDAO couponDAO = new CouponDAO(connection);
			int editArea = couponDAO.selectCouponEditArea();
			if (editArea >= id) {
				session.setAttribute("errorMsg", "不適切な値が入力されました。");
				response.sendRedirect("Coupon");
				return;
			}
			CouponDTO coupon = couponDAO.selectCoupon(id);
			request.setAttribute("coupon", coupon);
			request.getRequestDispatcher("WEB-INF/jsp/couponDetail.jsp").forward(request, response);
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystenError.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null || (Integer) session.getAttribute("auth") != 1) {
			response.sendRedirect("login");
			return;
		}
		
		request.setCharacterEncoding("utf-8");
		String couponId = request.getParameter("id");
		String content = request.getParameter("content");
		String discountRate = request.getParameter("discountRate");
		String activeFrom = request.getParameter("activeFrom");
		String activeUntil = request.getParameter("activeUntil");
		
		if (!Validate.isStrParseInt(couponId) || Validate.isEmpty(content) || !Validate.isStrParseInt(discountRate) 
				|| Validate.isEmpty(activeFrom) || Validate.isEmpty(activeUntil)) {
			session.setAttribute("errorMsg", "不適切な値が入力されました。");
			response.sendRedirect("Coupon");
			return;
		}
		
		CouponDTO couponDTO = new CouponDTO();
		couponDTO.setId(Integer.parseInt(couponId));
		couponDTO.setContent(content);
		couponDTO.setDiscountRate(Integer.parseInt(discountRate));
		couponDTO.setActiveFrom(activeFrom);
		couponDTO.setActiveUntil(activeUntil);
		
		try (Connection connection = DataSourceManager.getConnection()) {
			CouponDAO couponDAO = new CouponDAO(connection);
			int editArea = couponDAO.selectCouponEditArea();
			if (editArea < couponDTO.getId()) {
				session.setAttribute("errorMsg", "不適切な値が入力されました。");
				response.sendRedirect("Coupon");
				return;
			}
			if (couponDAO.updateCoupon(couponDTO) == 1) {
				session.setAttribute("msg", "クーポンの編集が完了しました");
				response.sendRedirect("Coupon");
				return;
			}
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystenError.jsp").forward(request, response);
			return;
		}
		
		session.setAttribute("errorMsg", "不適切な値が入力されました。");
		response.sendRedirect("Coupon");
	}
}
