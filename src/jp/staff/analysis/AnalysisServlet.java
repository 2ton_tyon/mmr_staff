package jp.staff.analysis;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.dao.AnalysisDAO;
import jp.staff.dao.CouponDAO;
import jp.staff.dto.AnalysisDTO;
import jp.staff.dto.CouponDTO;

/**
 * Servlet implementation class AnalysisServlet
 */
@WebServlet("/DataAnalysis")
public class AnalysisServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null || (Integer)session.getAttribute("auth") != 1) {
			response.sendRedirect("login");
			return;
		}
		
		// default値:18-29歳の今年のデータ
		ArrayList<String> yearList = new ArrayList<>();
		yearList.add("2015");
		yearList.add("2016");
		yearList.add("2017");
		ArrayList<String> ageList = new ArrayList<>();
		ageList.add("18歳-29歳");
		ageList.add("30代");
		ageList.add("40代");
		ageList.add("50代");
		ageList.add("60代-");
		
		try (Connection connection = DataSourceManager.getConnection()) {
			AnalysisDAO analysisDAO = new AnalysisDAO(connection);
			ArrayList<AnalysisDTO> analysis = analysisDAO.selectData("2017",18,29);
			request.setAttribute("analysis", analysis);
			CouponDAO couponDAO = new CouponDAO(connection);
			ArrayList<CouponDTO> couponList = couponDAO.selectCouponDiscountRate("2017");
			request.setAttribute("coupon", couponList);
			request.setAttribute("year", "2017");
			request.setAttribute("yearList", yearList);
			request.setAttribute("age", "1");
			request.setAttribute("ageList", ageList);
			request.getRequestDispatcher("WEB-INF/jsp/dataAnalysis.jsp").forward(request, response);
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null || (Integer)session.getAttribute("auth") != 1) {
			response.sendRedirect("login");
			return;
		}
		
		ArrayList<String> yearList = new ArrayList<>();
		yearList.add("2015");
		yearList.add("2016");
		yearList.add("2017");
		ArrayList<String> ageList = new ArrayList<>();
		ageList.add("18歳-29歳");
		ageList.add("30代");
		ageList.add("40代");
		ageList.add("50代");
		ageList.add("60代-");
		
		String year = request.getParameter("year");
		String age = request.getParameter("age");
		int ageTable[][] = {{18,29},{30,39},{40,49},{50,59},{60,200}};
		int ageNum = Integer.parseInt(age)-1;
		try (Connection connection = DataSourceManager.getConnection()) {
			AnalysisDAO analysisDAO = new AnalysisDAO(connection);
			ArrayList<AnalysisDTO> analysis = analysisDAO.selectData(year,ageTable[ageNum][0],ageTable[ageNum][1]);
			request.setAttribute("analysis", analysis);
			CouponDAO couponDAO = new CouponDAO(connection);
			ArrayList<CouponDTO> couponList = couponDAO.selectCouponDiscountRate(year);
			request.setAttribute("coupon", couponList);
			request.setAttribute("year", year);
			request.setAttribute("yearList", yearList);
			request.setAttribute("age", age);
			request.setAttribute("ageList", ageList);
			request.getRequestDispatcher("WEB-INF/jsp/dataAnalysis.jsp").forward(request, response);
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
		}
	}

}
