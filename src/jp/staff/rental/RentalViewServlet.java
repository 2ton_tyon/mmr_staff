package jp.staff.rental;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.Validate;
import jp.staff.dao.RentalDAO;
import jp.staff.dao.StaffDAO;
import jp.staff.dto.RentalDTO;
import jp.staff.dto.StaffDTO;
import jp.staff.dto.StatusDTO;

/**
 * Servlet implementation class RentalViewServlet
 */
@WebServlet("/rental")
public class RentalViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("id") == null) {
			response.sendRedirect("login");
			return;
		}
		// ページング処理のために現在のページ番号を取得する
		int page = 1;
		if(Validate.isStrParseInt(request.getParameter("page"))){
			page = Integer.parseInt(request.getParameter("page"));
		}
		
		try(Connection connection = DataSourceManager.getConnection()){
			RentalDAO rentalDAO = new RentalDAO(connection);
			ArrayList<RentalDTO> rentalList = rentalDAO.selectAll(page);
			int cntRental = rentalDAO.countAll();
			ArrayList<StatusDTO> statusList = rentalDAO.selectStatusAll();
			StaffDAO staffDAO = new StaffDAO(connection);
			ArrayList<StaffDTO> staffList = staffDAO.selectAll();
			request.setAttribute("page", page);
			request.setAttribute("cntRental", cntRental);
			request.setAttribute("statusList", statusList);
			request.setAttribute("rentalList", rentalList);
			request.setAttribute("staffList", staffList);
			request.getRequestDispatcher("WEB-INF/jsp/rentalView.jsp").forward(request, response);
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
		}
		
	}

}
