package jp.staff.rental;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.Validate;
import jp.staff.dao.RentalDAO;
import jp.staff.dto.RentalDetailDTO;

/**
 * Servlet implementation class RentalDetailServlet
 */
@WebServlet("/RentalDetail")
public class RentalDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null) {
			response.sendRedirect("login");
			return;
		}
		
		String rentalNumber = request.getParameter("rentalNumber");
		if(!Validate.isStrParseInt(rentalNumber)) {
			session.setAttribute("errorMessage", "問題が発生したためもう一度お願いします。");
			response.sendRedirect("rental");
			return;
		}
		int rentalId = Integer.parseInt(rentalNumber);
		
		try (Connection connection = DataSourceManager.getConnection()) {
			RentalDAO rentalDAO = new RentalDAO(connection);
			RentalDetailDTO rentalDetailDTO = rentalDAO.selectRentalDetail(rentalId);
			request.setAttribute("rentalDetail", rentalDetailDTO);
			request.getRequestDispatcher("WEB-INF/jsp/rentalDetail.jsp").forward(request, response);
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null) {
			response.sendRedirect("login");
			return;
		}
		
		String id = (String)session.getAttribute("id");
		
		String rentalNumber = request.getParameter("rentalNumber");
		if(!Validate.isStrParseInt(rentalNumber)) {
			session.setAttribute("errorMessage", "問題が発生したためもう一度お願いします。");
			response.sendRedirect("rental");
			return;
		}
		int rentalId = Integer.parseInt(rentalNumber);
		
		String rentalFlg = request.getParameter("rental");
		String returnFlg = request.getParameter("return");
		try (Connection connection = DataSourceManager.getConnection()) {
			if ("1".equals(rentalFlg) && !"1".equals(returnFlg)) {
				// 貸出処理
				// 1 入力された個体識別番号が最大個体識別番号よりも小さいかつ廃番ではないことを確認する
				RentalDAO rentalDAO = new RentalDAO(connection);
				
				String[] item = request.getParameterValues("itemId");
				String[] identification = request.getParameterValues("identificationNumber");
				
				ArrayList<Integer> itemIdList = new ArrayList<>();
				ArrayList<Integer> identificationNumberList = new ArrayList<>();
				if (item.length != identification.length) {
					session.setAttribute("errorMessage", "入力された個体識別番号は存在しません。");
					response.sendRedirect("RentalDetail?rentalNumber="+rentalId);
					return;
				}
				try {
					for ( int i = 0; i < item.length; i++) {
						itemIdList.add(Integer.parseInt(item[i]));
						identificationNumberList.add(Integer.parseInt(identification[i]));
					}
				} catch (NumberFormatException e) {
					session.setAttribute("errorMessage", "システムに不具合が発生しました。もう一度実行してください。");
					response.sendRedirect("RentalDetail?rentalNumber="+rentalId);
					return;
				}
				
				
				if (rentalDAO.isDisposalIdentificationNumber(itemIdList, identificationNumberList)) {
					session.setAttribute("errorMessage", "入力された個体識別番号は存在しません。");
					response.sendRedirect("RentalDetail?rentalNumber="+rentalId);
					return;
				}
				// 2 自動コミットを切る
				connection.setAutoCommit(false);
				// 3  ステータス,担当者のIDを更新する
				rentalDAO.updateRentalProcess(rentalId,id,2);
				// 4 商品ごとに入力された個体識別番号を更新する
				if(rentalDAO.updateItenIdentificationNumber(rentalId,itemIdList,identificationNumberList)) {
					// 5 コミットまたはロールバックを行う
					connection.commit();
				} else {
					connection.rollback();
				}
				response.sendRedirect("RentalDetail?rentalNumber="+rentalId);
			} else if (!"1".equals(rentalFlg) && "1".equals(returnFlg)) {
				// 返却処理
				RentalDAO rentalDAO = new RentalDAO(connection);
				rentalDAO.updateReturnProcess(rentalId,id,3);
				response.sendRedirect("RentalDetail?rentalNumber="+rentalId);
			} else {
				// 不適切な値の入力
				session.setAttribute("errorMessage", "システムに不具合が発生しました。もう一度実行してください。");
				response.sendRedirect("RentalDetail?rentalNumber="+rentalId);
			}
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
		}
	}

}
