package jp.staff.rental;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.Validate;
import jp.staff.dao.RentalDAO;
import jp.staff.dao.StaffDAO;
import jp.staff.dto.RentalDTO;
import jp.staff.dto.StaffDTO;
import jp.staff.dto.StatusDTO;

/**
 * Servlet implementation class SearchRentalViewServlet
 */
@WebServlet("/SearchRentalView")
public class SearchRentalViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		if(session.getAttribute("id") == null) {
			response.sendRedirect("login");
			return;
		}
		// ページング処理のために現在のページ番号を取得する
		int page = 1;
		if(Validate.isStrParseInt(request.getParameter("page"))){
			page = Integer.parseInt(request.getParameter("page"));
		}
		String orderDate = request.getParameter("orderDate");
		String returnDatePlan = request.getParameter("returnDatePlan");
		String returnDate = request.getParameter("returnDate");
		String rentalStaff = request.getParameter("rentalStaff");
		String returnStaff = request.getParameter("returnStaff");
		String status = request.getParameter("status");
		
		// 整合性用のフラグ
		boolean consistencyFlg = true;
		// エラーメッセージ
		ArrayList<String> errorMsg = new ArrayList<>();
		
		RentalDTO rentalDTO = new RentalDTO();
		if (Validate.isEmpty(orderDate) || "%".equals(orderDate)) {
			// 入力されていないので全件対応
			rentalDTO.setOrderDatetime("%");
		} else if (Validate.isStrParseDate(orderDate)) {
			// 入力された日付が正しい
			rentalDTO.setOrderDatetime(orderDate);
		} else {
			consistencyFlg = false;
			errorMsg.add("入力された注文日が存在しない値です");
		}
		
		if (Validate.isEmpty(returnDatePlan) || "%".equals(returnDatePlan)) {
			// 入力されていないので全件対応
			rentalDTO.setReturnPlanDate("%");
		} else if (Validate.isStrParseDate(returnDatePlan)) {
			// 入力された日付が正しい
			rentalDTO.setReturnPlanDate(returnDatePlan);
		} else {
			consistencyFlg = false;
			errorMsg.add("入力された返却予定日が存在しない値です");
		}
		
		if (Validate.isEmpty(returnDate) || "%".equals(returnDate)) {
			rentalDTO.setReturnDate("%");
		} else if (Validate.isStrParseDate(returnDate)) {
			rentalDTO.setReturnDate(returnDate);
		} else {
			consistencyFlg = false;
			errorMsg.add("入力された返却日が存在しない値です");
		}
		
		if (Validate.isEmpty(rentalStaff) || "%".equals(rentalStaff)) {
			rentalDTO.setRentalStaffId("%");
		} else if ("---".equals(rentalStaff)){
			rentalDTO.setRentalStaffId("%");
		} else {
			rentalDTO.setRentalStaffId(rentalStaff);
		}
		
		
		if (Validate.isEmpty(returnStaff) || "%".equals(returnStaff)) {
			rentalDTO.setReturnStaffId("%");
		} else if ("---".equals(returnStaff)){
			rentalDTO.setReturnStaffId("%");
		} else {
			rentalDTO.setReturnStaffId(returnStaff);
		}
		
		if (Validate.isEmpty(status) || "%".equals(status)) {
			rentalDTO.setStatusId("%");
		} else if ("---".equals(status)) {
			rentalDTO.setStatusId("%");
		} else if (Validate.isStrParseInt(status)) {
			rentalDTO.setStatusId(status);
		} else {
			consistencyFlg = false;
			errorMsg.add("入力されたステータスは存在しない値です");
		}
		
		if (!consistencyFlg) {
			session.setAttribute("errorMsg", errorMsg);
			response.sendRedirect("rental");
			return;
		}
		
		try (Connection connection = DataSourceManager.getConnection()) {
			RentalDAO rentalDAO = new RentalDAO(connection);
			ArrayList<RentalDTO> rentalList = rentalDAO.searchRental(page,rentalDTO);
			int cntRental = rentalDAO.searchCount(rentalDTO);
			ArrayList<StatusDTO> statusList = rentalDAO.selectStatusAll();
			StaffDAO staffDAO = new StaffDAO(connection);
			ArrayList<StaffDTO> staffList = staffDAO.selectAll();
			request.setAttribute("page", page);
			request.setAttribute("cntRental", cntRental);
			request.setAttribute("statusList", statusList);
			request.setAttribute("rentalList", rentalList);
			request.setAttribute("staffList", staffList);
			request.setAttribute("search", rentalDTO);
		} catch (SQLException | SQLRuntimeException e) {
			e.printStackTrace();
			request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
			return;
		}
		request.getRequestDispatcher("WEB-INF/jsp/rentalView.jsp").forward(request, response);
	}

}
