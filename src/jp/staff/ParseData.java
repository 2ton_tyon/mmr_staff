package jp.staff;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ParseData {
	
	
	/**
	 * 入力された日付を指定のフォーマットに変換する
	 * @param date　日付
	 * @return 文字列
	 */
	public static String parseDateToString(Date date){
		String str;
		String datePattern ="yyyy-MM-dd";
		if(date == null) {
			str = null;
		} else {
			str = new SimpleDateFormat(datePattern).format(date);
		}
		return str;
	}
	
	/**
	 * ミリ秒まで設定できるタイムスタンプ型を指定のフォーマットに変換する
	 * @param timestamp 日付
	 * @return 文字列
	 */
	public static String parseTimestampToString(Timestamp timestamp){
		String str;
		String datePattern ="yyyy-MM-dd HH:mm:ss";
		if(timestamp == null) {
			str = null;
		} else {
			str = new SimpleDateFormat(datePattern).format(timestamp);
		}
		return str;
	}
	
	/**
	 * 入力された文字列で構成される配列をリストに変換
	 * @param str　文字列配列
	 * @return　文字列リスト
	 */
	public static ArrayList<String> parseStringListStringArray (String[] str) {
		ArrayList<String> strList = new ArrayList<>();
		if (str == null) {
			return null;
		}
		for (int i = 0; i < str.length; i++) {
			strList.add(str[i]);
		}
		return strList;
	}
	
	/**
	 * 入力された文字列で構成される配列を数値リストに変換
	 * @param str　文字列配列
	 * @return　文字列リスト
	 */
	public static ArrayList<Integer> parseStringListToIntegerArray (String[] str) {
		if (str == null) {
			return null;
		}
		ArrayList<Integer> integers = new ArrayList<>();
		for (int i = 0; i <str.length; i++) {
			integers.add(Integer.parseInt(str[i]));
		}
		return integers;
	}
}
