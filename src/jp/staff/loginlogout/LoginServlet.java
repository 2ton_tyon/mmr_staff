package jp.staff.loginlogout;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.Validate;
import jp.staff.dao.StaffDAO;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String initPassword = "MMR_ONLINE";
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		HttpSession session = request.getSession();
		
		if (Validate.isEmpty(id) || Validate.isEmpty(password) || !(Validate.isRgularExpression(password, "^[0-9a-zA-Z_]{8,41}+$"))) {
				request.setAttribute("errorMessage", "従業員IDまたはパスワードが正しく入力されていません");
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}
		
		
		try(Connection conn = DataSourceManager.getConnection()){
			StaffDAO staffDAO = new StaffDAO(conn);
			if(!staffDAO.loginCheckMMRStaff(id, password)){
				request.setAttribute("errorMessage", "従業員IDまたはパスワードが間違っています");
				request.getRequestDispatcher("login.jsp").forward(request, response);
				return;
			}
			Integer auth = staffDAO.selectByAuth(id);
			if (auth == null) {
				request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
				return;
			} else {
				// ユーザの権限を格納する
				session.setAttribute("auth", auth);
			}
			//　ユーザのIDを格納する
			session.setAttribute("id", id);
			if(initPassword.equals(password)){
				// 初期パスワードでログインしたユーザは設定のページに遷移させる
				request.setAttribute("updateFlag", true);
				request.getRequestDispatcher("WEB-INF/jsp/reRegistPassword.jsp").forward(request, response);
			}else{
				response.sendRedirect("rental");
			}
			
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
		}
	}
}
