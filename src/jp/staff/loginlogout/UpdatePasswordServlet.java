package jp.staff.loginlogout;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.Validate;
import jp.staff.dao.StaffDAO;

/**
 * Servlet implementation class UpdatePasswordServlet
 */
@WebServlet("/updatePassword")
public class UpdatePasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getAttribute("updateFlag") == null) {
			response.sendRedirect("login");
			return;
		}
		request.getRequestDispatcher("/WEB-INF/jsp/reRegistPassword.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String initPassword = "MMR_ONLINE";
		String pass1 = request.getParameter("password");
		String pass2 = request.getParameter("confirmPassword");
		
		HttpSession session = request.getSession();
		
		String id = (String) session.getAttribute("id");
		
		if (Validate.isEmpty(pass1) || Validate.isEmpty(pass2) || !(Validate.isRgularExpression(pass1, "^[0-9a-zA-Z_]{8,41}+$"))) {
			request.setAttribute("errorMessage", "不適切な値が入力されました");
			request.getRequestDispatcher("/WEB-INF/jsp/reRegistPassword.jsp").forward(request, response);
			return;
		}
		
		if (initPassword.equals(pass1)) {
			request.setAttribute("errorMessage", "パスワードを新たに入力してください");
			request.getRequestDispatcher("/WEB-INF/jsp/reRegistPassword.jsp").forward(request, response);
			return;
		}
		
						
		if(!pass2.equals(pass1)){
			request.setAttribute("errorMessage", "同じパスワードを入力してください");
			request.getRequestDispatcher("/WEB-INF/jsp/reRegistPassword.jsp").forward(request, response);
			return;
		}else{
			try(Connection conn = DataSourceManager.getConnection()){
				StaffDAO dao = new StaffDAO(conn);
				if(!dao.reRegistPassword(pass1, id)){
					session.setAttribute("errorMessage", "パスワードの登録に失敗しました。もう一度お願いします");
					request.getRequestDispatcher("/WEB-INF/jsp/reRegistPassword.jsp").forward(request, response);
					return;
				}	
			} catch (SQLException | SQLRuntimeException e) {
				request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
				return;
			}
		}
		response.sendRedirect("login");
	}
}
