package jp.staff.dao;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import jp.staff.SQLRuntimeException;
import jp.staff.dto.CategoryDTO;
import jp.staff.dto.GenreDTO;
import jp.staff.dto.ItemDTO;

public class ItemDAO {
	
	private Connection connection;
	/**
	 * コネクションをセットする
	 * @param conn
	 */
	public ItemDAO(Connection connection) {
		this.connection = connection;
	}
	
	public ArrayList<ItemDTO> selectALL() throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ITEM_ID");
		sb.append("        ,ITEM.NEW_AND_OLD_ID");
		sb.append("        ,NEW_AND_OLD.NEW_AND_OLD_NAME");
		sb.append("        ,ITEM.CATEGORY_ID");
		sb.append("        ,CATEGORY.CATEGORY_NAME");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,ARTIST");
		sb.append("        ,PRICE");
		sb.append("    FROM");
		sb.append("        ITEM");
		sb.append("            LEFT JOIN NEW_AND_OLD");
		sb.append("                ON NEW_AND_OLD.NEW_AND_OLD_ID = ITEM.NEW_AND_OLD_ID");
		sb.append("            LEFT JOIN CATEGORY");
		sb.append("                ON CATEGORY.CATEGORY_ID = ITEM.CATEGORY_ID");
		
		ArrayList<ItemDTO> itemList = new ArrayList<>();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				ItemDTO itemRowData = new ItemDTO();
				itemRowData.setItem_id(rs.getInt("ITEM_ID"));
				itemRowData.setNewAndOldId(rs.getInt("ITEM.NEW_AND_OLD_ID"));
				itemRowData.setNewAndOldName(rs.getString("NEW_AND_OLD.NEW_AND_OLD_NAME"));
				itemRowData.setCategoryId(rs.getInt("ITEM.CATEGORY_ID"));
				itemRowData.setCategoryName(rs.getString("CATEGORY.CATEGORY_NAME"));
				itemRowData.setItemName(rs.getString("ITEM_NAME"));
				itemRowData.setArtist(rs.getString("ARTIST"));
				itemRowData.setPrice(rs.getInt("PRICE"));
				itemList.add(itemRowData);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		
		return itemList;
	}
	
	public BufferedImage selectImageById(int id) throws SQLRuntimeException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   select");
		sb.append("          image");
		sb.append("     from item");
		sb.append("    where item_id = ?");
		
		try {
			// SQL文を実行する
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
		
			BufferedInputStream bis = null;
			// 結果セットから画像データを取得し、返却する
			if (rs.next()) {
				InputStream is = rs.getBinaryStream("image");
				bis = new BufferedInputStream(is);
			}
			return ImageIO.read(bis);

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} catch (IOException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	public ArrayList<ItemDTO> selectSearchItem(String category, String genre, String title) throws SQLRuntimeException {
		
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ITEM_ID");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,ITEM.CATEGORY_ID");
		sb.append("        ,CATEGORY.CATEGORY_NAME");
		sb.append("        ,ITEM.NEW_AND_OLD_ID");
		sb.append("        ,NEW_AND_OLD.NEW_AND_OLD_NAME");
		sb.append("        ,ARTIST");
		sb.append("        ,PRICE");
		sb.append("    FROM");
		sb.append("        ITEM");
		sb.append("            LEFT JOIN CATEGORY");
		sb.append("                ON ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID");
		sb.append("            LEFT JOIN NEW_AND_OLD");
		sb.append("                ON ITEM.NEW_AND_OLD_ID = NEW_AND_OLD.NEW_AND_OLD_ID");
		sb.append("    WHERE");
		sb.append("        ITEM_ID IN (");
		sb.append("            SELECT");
		sb.append("                    DISTINCT ITEM.ITEM_ID");
		sb.append("                FROM");
		sb.append("                    ITEM_GENRE");
		sb.append("                        LEFT JOIN ITEM");
		sb.append("                            ON ITEM_GENRE.ITEM_ID = ITEM.ITEM_ID");
		sb.append("                        LEFT JOIN CATEGORY");
		sb.append("                            ON ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID");
		sb.append("                        LEFT JOIN GENRE");
		sb.append("                            ON GENRE.GENRE_ID = ITEM_GENRE.GENRE_ID");
		sb.append("                WHERE");
		sb.append("                    ITEM.ITEM_NAME LIKE ?");
		sb.append("                    AND CATEGORY.CATEGORY_NAME = ?");
		sb.append("                    AND GENRE.GENRE_NAME = ?");
		sb.append("        )");
		sb.append("    ORDER BY");
		sb.append("        ITEM_ID DESC");
		
		ArrayList<ItemDTO> itemList = new ArrayList<>();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1,  String.format("%1$s%2$s%1$s", "%", title));
			ps.setString(2, category);
			ps.setString(3, genre);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				ItemDTO item = new ItemDTO();
				
				item.setItem_id(rs.getInt("ITEM_ID"));
				item.setItemName(rs.getString("ITEM_NAME"));
				item.setCategoryId(rs.getInt("ITEM.CATEGORY_ID"));
				item.setCategoryName(rs.getString("CATEGORY.CATEGORY_NAME"));
				item.setNewAndOldId(rs.getInt("ITEM.NEW_AND_OLD_ID"));
				item.setNewAndOldName(rs.getString("NEW_AND_OLD.NEW_AND_OLD_NAME"));
				item.setArtist(rs.getString("ARTIST"));
				item.setPrice(rs.getInt("PRICE"));
				
				itemList.add(item);
			}
			
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return itemList;
	}
	
	public ItemDTO selectItemDetail (Integer itemId) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ITEM.ITEM_ID");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,CATEGORY.CATEGORY_ID");
		sb.append("        ,CATEGORY.CATEGORY_NAME");
		sb.append("        ,RECOMMENDED_FLG");
		sb.append("        ,ITEM.NEW_AND_OLD_ID");
		sb.append("        ,NEW_AND_OLD.NEW_AND_OLD_NAME");
		sb.append("        ,ARTIST");
		sb.append("        ,PRICE");
		sb.append("        ,REMARKS");
		sb.append("        ,MAX_IDENTIFICATION_NUMBER");
		sb.append("    FROM");
		sb.append("        ITEM");
		sb.append("            LEFT JOIN CATEGORY");
		sb.append("                ON ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID");
		sb.append("            LEFT JOIN NEW_AND_OLD");
		sb.append("                ON ITEM.NEW_AND_OLD_ID = NEW_AND_OLD.NEW_AND_OLD_ID");
		sb.append("    WHERE");
		sb.append("        ITEM_ID = ?");
		
		// 商品のジャンルを取得する
		StringBuffer sb2 = new StringBuffer();
		sb2.append("   SELECT");
		sb2.append("          GENRE.GENRE_NAME");
		sb2.append("    FROM ITEM_GENRE");
		sb2.append("    LEFT JOIN GENRE");
		sb2.append("    ON GENRE.GENRE_ID = ITEM_GENRE.GENRE_ID");
		sb2.append("    WHERE ITEM_ID = ?");
		
		// 廃番リストを取得する
		StringBuffer sb3 = new StringBuffer();
		sb3.append("SELECT");
		sb3.append("        IDENTIFICATION_NUMBER");
		sb3.append("    FROM");
		sb3.append("        DISPOSAL_IDENTIFICATION_NUMBER");
		sb3.append("    WHERE");
		sb3.append("        ITEM_ID = ?");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			PreparedStatement ps2 = connection.prepareStatement(sb2.toString());
			PreparedStatement ps3 = connection.prepareStatement(sb3.toString());
			
			ps.setInt(1, itemId);
			ps2.setInt(1, itemId);
			ps3.setInt(1, itemId);
			
			ResultSet rs = ps.executeQuery();
			ItemDTO item = new ItemDTO();
			if (rs.next()) {
				item.setItem_id(rs.getInt("ITEM.ITEM_ID"));
				item.setItemName(rs.getString("ITEM_NAME"));
				item.setRecommendedFlag(rs.getInt("RECOMMENDED_FLG"));
				item.setCategoryId(rs.getInt("CATEGORY.CATEGORY_ID"));
				item.setCategoryName(rs.getString("CATEGORY.CATEGORY_NAME"));
				item.setNewAndOldId(rs.getInt("ITEM.NEW_AND_OLD_ID"));
				item.setNewAndOldName(rs.getString("NEW_AND_OLD.NEW_AND_OLD_NAME"));
				item.setArtist(rs.getString("ARTIST"));
				item.setPrice(rs.getInt("PRICE"));
				item.setRemarks(rs.getString("REMARKS"));
				item.setMaxIdentificationNumber(rs.getInt("MAX_IDENTIFICATION_NUMBER"));
			}
			
			rs = ps2.executeQuery();
			ArrayList<String> genreList = new ArrayList<>();
			
			while (rs.next()) {
				genreList.add(rs.getString("GENRE.GENRE_NAME"));
			}
			
			item.setGenreName(genreList);
			
			rs = ps3.executeQuery();
			
			ArrayList<Integer> disposalIdentificationNumberList = new ArrayList<>();
			while (rs.next()) {
				disposalIdentificationNumberList.add(rs.getInt("IDENTIFICATION_NUMBER"));
			}
			item.setDisposalIdentificationNumber(disposalIdentificationNumberList);
			return item;	
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	public int updateItem(ItemDTO itemDTO) throws SQLRuntimeException {
		// ITEM TABLEの情報を更新する
		
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        ITEM");
		sb.append("    SET");
		sb.append("        ITEM_NAME = ?");
		sb.append("        ,CATEGORY_ID = ?");
		sb.append("        ,RECOMMENDED_FLG = ?");
		sb.append("        ,NEW_AND_OLD_ID = ?");
		sb.append("        ,ARTIST = ?");
		sb.append("        ,PRICE = ?");
		sb.append("        ,REMARKS = ?");
		sb.append("        ,MAX_IDENTIFICATION_NUMBER = ?");
		sb.append("    WHERE");
		sb.append("        ITEM_ID = ?");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, itemDTO.getItemName());
			ps.setInt(2, itemDTO.getCategoryId());
			ps.setInt(3, itemDTO.getRecommendedFlag());
			ps.setInt(4, itemDTO.getNewAndOldId());
			ps.setString(5, itemDTO.getArtist());
			ps.setInt(6, itemDTO.getPrice());
			ps.setString(7, itemDTO.getRemarks());
			ps.setInt(8, itemDTO.getMaxIdentificationNumber());
			ps.setInt(9, itemDTO.getItem_id());
			
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	public boolean deleteItemGenre(Integer id) throws SQLRuntimeException {
		
		//　現在のITEM_GENRE TABLEの情報を破棄する
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE");
		sb.append("    FROM");
		sb.append("        ITEM_GENRE");
		sb.append("    WHERE");
		sb.append("        ITEM_ID = ?");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return true;
	}
		
	public int insertItemGenre(ItemDTO itemDTO) throws SQLRuntimeException {
		
		// ITEM_GENRE TABLEの情報を追加する
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        ITEM_GENRE(");
		sb.append("            GENRE_ID");
		sb.append("            ,ITEM_ID");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,?");
		sb.append("        );");
		int cnt = 0;
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(2, itemDTO.getItem_id());
			for (Integer genreId : itemDTO.getGenreId()) {
				ps.setInt(1,genreId);
				cnt += ps.executeUpdate();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return cnt;
	}
	
	public boolean deleteDisposalIdentificationNumber(Integer id) throws SQLRuntimeException {
		// 現在のDISPOSALNUMBER TABLEから情報を削除する
		StringBuffer sb = new StringBuffer();
		sb.append("DELETE");
		sb.append("    FROM");
		sb.append("        DISPOSAL_IDENTIFICATION_NUMBER");
		sb.append("    WHERE");
		sb.append("        ITEM_ID = ?");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return true;
	}
	
	public boolean insertDisposalIdentificationNumber(ItemDTO itemDTO) throws SQLRuntimeException {
		
		if (itemDTO.getDisposalIdentificationNumber() == null) {
			return true;
		}
		// DISPOSALNUMBER TABLEの情報を更新する
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        DISPOSAL_IDENTIFICATION_NUMBER");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,?");
		sb.append("        )");
		int cnt = 0;
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, itemDTO.getItem_id());
			for (Integer i : itemDTO.getDisposalIdentificationNumber()) {
				ps.setInt(2, i);
				cnt += ps.executeUpdate();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		if (itemDTO.getDisposalIdentificationNumber().size() == cnt) {
			return true;
		} else {
			return false;
		}
	}
	
	public ArrayList<CategoryDTO> selectCategory() throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        CATEGORY_ID");
		sb.append("        ,CATEGORY_NAME");
		sb.append("    FROM");
		sb.append("        CATEGORY;");
		
		ArrayList<CategoryDTO> categoryList = new ArrayList<>();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ResultSet rs = ps.executeQuery();
			// SQLの結果を取得し、リストに詰める
			while (rs.next()) {
				CategoryDTO categoryDTO = new CategoryDTO();
				categoryDTO.setId(rs.getInt("CATEGORY_ID"));
				categoryDTO.setName(rs.getString("CATEGORY_NAME"));
				categoryList.add(categoryDTO);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return categoryList;
	}
	
	public ArrayList<GenreDTO> selectGenre() throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        GENRE_ID");
		sb.append("        ,GENRE_NAME");
		sb.append("        ,CATEGORY_ID");
		sb.append("    FROM");
		sb.append("        GENRE;");
		
		ArrayList<GenreDTO> genreList =  new ArrayList<>();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ResultSet rs = ps.executeQuery();
			// SQLの結果を取得し、リストに詰める
			while (rs.next()) {
				GenreDTO genreDTO = new GenreDTO();
				genreDTO.setId(rs.getInt("GENRE_ID"));
				genreDTO.setName(rs.getString("GENRE_NAME"));
				genreDTO.setCategoryId(rs.getInt("CATEGORY_ID"));
				genreList.add(genreDTO);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return genreList;
	}
	
	public int insertItem(ItemDTO itemDTO) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        ITEM(");
		sb.append("            ITEM_NAME");
		sb.append("            ,CATEGORY_ID");
		sb.append("            ,RECOMMENDED_FLG");
		sb.append("            ,NEW_AND_OLD_ID");
		sb.append("            ,ARTIST");
		sb.append("            ,PRICE");
		sb.append("            ,REMARKS");
		sb.append("            ,MAX_IDENTIFICATION_NUMBER");
		sb.append("            ,CREATE_DATE");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,current_timestamp()");
		sb.append("        );");
		
		StringBuffer sb2 = new StringBuffer();
		sb2.append("SELECT");
		sb2.append("        last_insert_id() AS ID");
		sb2.append("    FROM");
		sb2.append("        item limit 1;");

		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			PreparedStatement ps2 = connection.prepareStatement(sb2.toString());
			ps.setString(1, itemDTO.getItemName());
			ps.setInt(2, itemDTO.getCategoryId());
			ps.setInt(3, itemDTO.getRecommendedFlag());
			ps.setInt(4, itemDTO.getNewAndOldId());
			ps.setString(5, itemDTO.getArtist());
			ps.setInt(6, itemDTO.getPrice());
			ps.setString(7, itemDTO.getRemarks());
			ps.setInt(8, itemDTO.getMaxIdentificationNumber());
			if(ps.executeUpdate() == 1){
				ResultSet rs = ps2.executeQuery();
				if (rs.next()) {
					return rs.getInt("ID");
				}
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return 0;

	}
}
