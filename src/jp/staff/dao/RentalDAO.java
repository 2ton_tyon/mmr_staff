package jp.staff.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.staff.ParseData;
import jp.staff.SQLRuntimeException;
import jp.staff.dto.RentalDTO;
import jp.staff.dto.RentalDetailDTO;
import jp.staff.dto.RentalItemDTO;
import jp.staff.dto.StatusDTO;

public class RentalDAO {
	protected Connection connection;
	public RentalDAO(Connection connection){
		this.connection = connection;
	} 
	
	public ArrayList<RentalDTO> selectAll(int pageNo) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        RENTAL.RENTAL_NUMBER");
		sb.append("        ,ORDER_DATETIME");
		sb.append("        ,RETURN_DATE_PLAN");
		sb.append("        ,RETURN_DATE");
		sb.append("        ,COUNT(RENTAL_DETAIL.RENTAL_NUMBER) AS CNT");
		sb.append("        ,RENTAL_STAFF_ID");
		sb.append("        ,RETURN_STAFF_ID");
		sb.append("        ,RENTAL.STATUS_ID");
		sb.append("        ,STATUS.STATUS_NAME");
		sb.append("    FROM");
		sb.append("        RENTAL INNER JOIN RENTAL_DETAIL");
		sb.append("            ON RENTAL.RENTAL_NUMBER = RENTAL_DETAIL.RENTAL_NUMBER");
		sb.append("        LEFT JOIN STATUS");
		sb.append("            ON RENTAL.STATUS_ID = STATUS.STATUS_ID");
		sb.append(" GROUP BY");
		sb.append("    RENTAL.RENTAL_NUMBER");
		sb.append("    ,ORDER_DATETIME");
		sb.append("    ,RETURN_DATE_PLAN");
		sb.append("    ,RETURN_DATE");
		sb.append("    ,RENTAL_STAFF_ID");
		sb.append("    ,RETURN_STAFF_ID");
		sb.append("    ,RENTAL.STATUS_ID");
		sb.append("    ,STATUS.STATUS_NAME ORDER BY RENTAL.RENTAL_NUMBER DESC LIMIT ?,20");
		
		StringBuffer sb2 = new StringBuffer();
		sb2.append("SELECT");
		sb2.append("        NAME");
		sb2.append("    FROM");
		sb2.append("        STAFF");
		sb2.append("    WHERE");
		sb2.append("        STAFF_ID = ?");

		
		
		ArrayList<RentalDTO> rentalList = new ArrayList<RentalDTO>();
		
		try{
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, (pageNo-1)*20);
			PreparedStatement ps2 = connection.prepareStatement(sb2.toString());
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				RentalDTO rentalRowData = new RentalDTO();
				rentalRowData.setRentalNumber(rs.getInt("RENTAL.RENTAL_NUMBER"));
				rentalRowData.setOrderDatetime(ParseData.parseTimestampToString(rs.getTimestamp("ORDER_DATETIME")));
				rentalRowData.setReturnPlanDate(ParseData.parseDateToString(rs.getDate("RETURN_DATE_PLAN")));
				rentalRowData.setReturnDate(ParseData.parseDateToString(rs.getDate("RETURN_DATE")));
				rentalRowData.setItmeCount(rs.getInt("CNT"));
				rentalRowData.setRentalStaffId(rs.getString("RENTAL_STAFF_ID"));
				rentalRowData.setReturnStaffId(rs.getString("RETURN_STAFF_ID"));
				rentalRowData.setStatusId(rs.getString("RENTAL.STATUS_ID"));
				rentalRowData.setStatus(rs.getString("STATUS.STATUS_NAME"));
				ps2.setString(1, rs.getString("RENTAL_STAFF_ID"));
				ResultSet rs2 = ps2.executeQuery();
				if (rs2.next()) {
					rentalRowData.setRentalStaff(rs2.getString("NAME"));
				}
				ps2.setString(1, rs.getString("RETURN_STAFF_ID"));
				rs2 = ps2.executeQuery();
				if (rs2.next()) {
					rentalRowData.setReturnStaff(rs2.getString("NAME"));
				}
				rentalList.add(rentalRowData);
			}
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return rentalList;
	}
	
	public int countAll() throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUNT(*) AS CNT");
		sb.append("    FROM");
		sb.append("        RENTAL");
		
		try{
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				return rs.getInt("CNT");
			}
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return 0;
	}
	
	public ArrayList<StatusDTO> selectStatusAll() throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        STATUS_ID");
		sb.append("        ,STATUS_NAME");
		sb.append("    FROM");
		sb.append("        STATUS");
		
		ArrayList<StatusDTO> statusList = new ArrayList<>();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				StatusDTO statusDTO = new StatusDTO();
				statusDTO.setId(rs.getInt("STATUS_ID"));
				statusDTO.setName(rs.getString("STATUS_NAME"));
				statusList.add(statusDTO);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return statusList;

	}
	
	public ArrayList<RentalDTO> searchRental (int page,RentalDTO rentalDTO) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("            RENTAL.RENTAL_NUMBER");
		sb.append("            ,ORDER_DATETIME");
		sb.append("            ,RETURN_DATE_PLAN");
		sb.append("            ,RETURN_DATE");
		sb.append("            ,");
		sb.append("            COUNT(RENTAL_DETAIL.RENTAL_NUMBER) AS CNT");
		sb.append("            ,RETURN_STAFF_ID");
		sb.append("            ,");
		sb.append("            RENTAL_STAFF_ID");
		sb.append("            ,");
		sb.append("            RENTAL.STATUS_ID");
		sb.append("            ,STATUS.STATUS_NAME");
		sb.append("        FROM");
		sb.append("            RENTAL INNER JOIN RENTAL_DETAIL");
		sb.append("                ON RENTAL.RENTAL_NUMBER = RENTAL_DETAIL.RENTAL_NUMBER");
		sb.append("            LEFT JOIN STATUS");
		sb.append("                ON RENTAL.STATUS_ID = STATUS.STATUS_ID");
		sb.append("    WHERE");
		sb.append("				ORDER_DATETIME LIKE ?");
		sb.append("        AND (");
		sb.append("            CASE");
		sb.append("                WHEN RETURN_DATE_PLAN IS NULL THEN ''");
		sb.append("                ELSE RETURN_DATE_PLAN");
		sb.append("            END");
		sb.append("        ) LIKE ?");
		sb.append("        AND (");
		sb.append("            CASE");
		sb.append("                WHEN RETURN_DATE IS NULL THEN ''");
		sb.append("                ELSE RETURN_DATE");
		sb.append("            END");
		sb.append("        ) LIKE ?");
		sb.append("        AND (");
		sb.append("            CASE");
		sb.append("                WHEN RENTAL_STAFF_ID IS NULL THEN ''");
		sb.append("                ELSE RENTAL_STAFF_ID");
		sb.append("            END");
		sb.append("        ) LIKE ?");
		sb.append("        AND (");
		sb.append("            CASE");
		sb.append("                WHEN RETURN_STAFF_ID IS NULL THEN ''");
		sb.append("                ELSE RETURN_STAFF_ID");
		sb.append("            END");
		sb.append("        ) LIKE ?");
		sb.append("        AND RENTAL.STATUS_ID LIKE ?");
		sb.append("    GROUP BY");
		sb.append("        RENTAL.RENTAL_NUMBER");
		sb.append("        ,ORDER_DATETIME");
		sb.append("        ,RETURN_DATE_PLAN");
		sb.append("        ,RETURN_DATE");
		sb.append("        ,RETURN_STAFF_ID");
		sb.append("        ,RENTAL_STAFF_ID");
		sb.append("        ,RENTAL.STATUS_ID ORDER BY RENTAL.RENTAL_NUMBER DESC LIMIT ?,20");
		
		StringBuffer sb2 = new StringBuffer();
		sb2.append("SELECT");
		sb2.append("        NAME");
		sb2.append("    FROM");
		sb2.append("        STAFF");
		sb2.append("    WHERE");
		sb2.append("        STAFF_ID = ?");
		
		ArrayList<RentalDTO> rentalList = new ArrayList<>();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			PreparedStatement ps2 = connection.prepareStatement(sb2.toString());
			ps.setString(1, rentalDTO.getOrderDatetime());
			ps.setString(2, rentalDTO.getReturnPlanDate());
			ps.setString(3, rentalDTO.getReturnDate());
			ps.setString(4, rentalDTO.getRentalStaffId());
			ps.setString(5, rentalDTO.getReturnStaffId());
			ps.setString(6, rentalDTO.getStatusId());
			ps.setInt(7,((page-1)*10));
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				RentalDTO rentalRowData = new RentalDTO();
				rentalRowData.setRentalNumber(rs.getInt("RENTAL.RENTAL_NUMBER"));
				rentalRowData.setOrderDatetime(ParseData.parseTimestampToString(rs.getTimestamp("ORDER_DATETIME")));
				rentalRowData.setReturnPlanDate(ParseData.parseDateToString(rs.getDate("RETURN_DATE_PLAN")));
				rentalRowData.setReturnDate(ParseData.parseDateToString(rs.getDate("RETURN_DATE")));
				rentalRowData.setItmeCount(rs.getInt("CNT"));
				rentalRowData.setRentalStaffId(rs.getString("RENTAL_STAFF_ID"));
				rentalRowData.setReturnStaffId(rs.getString("RETURN_STAFF_ID"));
				rentalRowData.setStatusId(rs.getString("RENTAL.STATUS_ID"));
				rentalRowData.setStatus(rs.getString("STATUS.STATUS_NAME"));
				ps2.setString(1, rs.getString("RENTAL_STAFF_ID"));
				ResultSet rs2 = ps2.executeQuery();
				if (rs2.next()) {
					rentalRowData.setRentalStaff(rs2.getString("NAME"));
				}
				ps2.setString(1, rs.getString("RETURN_STAFF_ID"));
				rs2 = ps2.executeQuery();
				if (rs2.next()) {
					rentalRowData.setReturnStaff(rs2.getString("NAME"));
				}
				rentalList.add(rentalRowData);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return rentalList;
	}
	
	
	public int searchCount (RentalDTO rentalDTO) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("            COUNT(*) AS CNT");
		sb.append("        FROM");
		sb.append("            RENTAL INNER JOIN RENTAL_DETAIL");
		sb.append("                ON RENTAL.RENTAL_NUMBER = RENTAL_DETAIL.RENTAL_NUMBER");
		sb.append("            LEFT JOIN STATUS");
		sb.append("                ON RENTAL.STATUS_ID = STATUS.STATUS_ID");
		sb.append("    WHERE");
		sb.append("				ORDER_DATETIME LIKE ?");
		sb.append("        AND (");
		sb.append("            CASE");
		sb.append("                WHEN RETURN_DATE_PLAN IS NULL THEN ''");
		sb.append("                ELSE RETURN_DATE_PLAN");
		sb.append("            END");
		sb.append("        ) LIKE ?");
		sb.append("        AND (");
		sb.append("            CASE");
		sb.append("                WHEN RETURN_DATE IS NULL THEN ''");
		sb.append("                ELSE RETURN_DATE");
		sb.append("            END");
		sb.append("        ) LIKE ?");
		sb.append("        AND (");
		sb.append("            CASE");
		sb.append("                WHEN RENTAL_STAFF_ID IS NULL THEN ''");
		sb.append("                ELSE RENTAL_STAFF_ID");
		sb.append("            END");
		sb.append("        ) LIKE ?");
		sb.append("        AND (");
		sb.append("            CASE");
		sb.append("                WHEN RETURN_STAFF_ID IS NULL THEN ''");
		sb.append("                ELSE RETURN_STAFF_ID");
		sb.append("            END");
		sb.append("        ) LIKE ?");
		sb.append("        AND RENTAL.STATUS_ID LIKE ?");
		sb.append("    GROUP BY");
		sb.append("        RENTAL.RENTAL_NUMBER");
		sb.append("        ,ORDER_DATETIME");
		sb.append("        ,RETURN_DATE_PLAN");
		sb.append("        ,RETURN_DATE");
		sb.append("        ,RETURN_STAFF_ID");
		sb.append("        ,RENTAL_STAFF_ID");
		sb.append("        ,RENTAL.STATUS_ID");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, rentalDTO.getOrderDatetime());
			ps.setString(2, rentalDTO.getReturnPlanDate());
			ps.setString(3, rentalDTO.getReturnDate());
			ps.setString(4, rentalDTO.getRentalStaffId());
			ps.setString(5, rentalDTO.getReturnStaffId());
			ps.setString(6, rentalDTO.getStatusId());
			ResultSet rs = ps.executeQuery();
			int cnt = 0;
			while(rs.next()) {
				cnt += 1;
			}
			return cnt;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	public RentalDetailDTO selectRentalDetail (int rentalNumber) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        RENTAL.RENTAL_NUMBER");
		sb.append("        ,COUNT(RENTAL_DETAIL.RENTAL_NUMBER) AS CNT");
		sb.append("        ,RENTAL.STATUS_ID");
		sb.append("        ,STATUS.STATUS_NAME");
		sb.append("    FROM");
		sb.append("        RENTAL INNER JOIN RENTAL_DETAIL");
		sb.append("            ON RENTAL.RENTAL_NUMBER = RENTAL_DETAIL.RENTAL_NUMBER");
		sb.append("        LEFT JOIN STATUS");
		sb.append("            ON RENTAL.STATUS_ID = STATUS.STATUS_ID");
		sb.append(" WHERE");
		sb.append("    RENTAL.RENTAL_NUMBER = ?");
		sb.append(" GROUP BY");
		sb.append("    RENTAL.RENTAL_NUMBER");
		sb.append("    ,ORDER_DATETIME");
		sb.append("    ,RENTAL.STATUS_ID");
		
		StringBuffer sb2 = new StringBuffer();
		sb2.append("SELECT");
		sb2.append("        RENTAL_DETAIL.ITEM_ID");
		sb2.append("        ,IDENTIFICATION_NUMBER");
		sb2.append("        ,ITEM_NAME");
		sb2.append("        ,CATEGORY.CATEGORY_NAME");
		sb2.append("        ,ITEM.ARTIST");
		sb2.append("        ,PRICE");
		sb2.append("        ,MAX_IDENTIFICATION_NUMBER");
		sb2.append("    FROM");
		sb2.append("        RENTAL_DETAIL");
		sb2.append("            LEFT JOIN ITEM");
		sb2.append("                ON RENTAL_DETAIL.ITEM_ID = ITEM.ITEM_ID");
		sb2.append("            LEFT JOIN CATEGORY");
		sb2.append("                ON CATEGORY.CATEGORY_ID = ITEM.CATEGORY_ID");
		sb2.append("    WHERE");
		sb2.append("        RENTAL_NUMBER = ?");

		StringBuffer sb3 = new StringBuffer();
		sb3.append("SELECT");
		sb3.append("        GENRE.GENRE_NAME");
		sb3.append("    FROM");
		sb3.append("        ITEM_GENRE");
		sb3.append("            LEFT JOIN GENRE");
		sb3.append("                ON GENRE.GENRE_ID = ITEM_GENRE.GENRE_ID");
		sb3.append("    WHERE");
		sb3.append("        ITEM_ID = ?");
		
		RentalDetailDTO rentalDetailDTO = new RentalDetailDTO();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			PreparedStatement ps2 = connection.prepareStatement(sb2.toString());
			PreparedStatement ps3 = connection.prepareStatement(sb3.toString());
			
			ps.setInt(1, rentalNumber);
			ps2.setInt(1, rentalNumber);
			
			ResultSet rs = ps.executeQuery(); 

			if (rs.next()) {
				rentalDetailDTO.setRentalNumber(rs.getInt("RENTAL.RENTAL_NUMBER"));
				rentalDetailDTO.setTotalItem(rs.getInt("CNT"));
				rentalDetailDTO.setStatusId(rs.getInt("RENTAL.STATUS_ID"));
				rentalDetailDTO.setStatusName(rs.getString("STATUS.STATUS_NAME"));
			}
			
			ArrayList<RentalItemDTO> rentalItemList = new ArrayList<>();
			rs = ps2.executeQuery();

			while (rs.next()) {
				RentalItemDTO rentalItem = new RentalItemDTO();
				rentalItem.setItemId(rs.getInt("RENTAL_DETAIL.ITEM_ID"));
				rentalItem.setIdentificationNumber(rs.getInt("IDENTIFICATION_NUMBER"));
				rentalItem.setMaxIdentificationNumber(rs.getInt("MAX_IDENTIFICATION_NUMBER"));
				rentalItem.setCategory(rs.getString("CATEGORY.CATEGORY_NAME"));
				rentalItem.setTitle(rs.getString("ITEM_NAME"));
				rentalItem.setArtist(rs.getString("ITEM.ARTIST"));
				rentalItem.setPrice(rs.getInt("PRICE"));
				
				ps3.setInt(1, rs.getInt("RENTAL_DETAIL.ITEM_ID"));
				ResultSet rs2 = ps3.executeQuery();
				ArrayList<String> genreList = new ArrayList<>();
				
				while (rs2.next()) {
					genreList.add(rs2.getString("GENRE.GENRE_NAME"));
				}
				rentalItem.setGenreList(genreList);
				rentalItemList.add(rentalItem);
			}
			
			rentalDetailDTO.setRentalItemList(rentalItemList);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		
		return rentalDetailDTO;
	}
	
	public void updateRentalProcess (int rentalNumber, String id, int statusId) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        RENTAL");
		sb.append("    SET");
		sb.append("        RENTAL_STAFF_ID = ?");
		sb.append("        ,STATUS_ID = ?");
		sb.append("    WHERE");
		sb.append("        RENTAL_NUMBER = ?");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, id);
			ps.setInt(2, statusId);
			ps.setInt(3, rentalNumber);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}		
	}
	
	public void updateReturnProcess (int rentalNumber, String id, int statusId) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        RENTAL");
		sb.append("    SET");
		sb.append("        RETURN_DATE = DATE(CURRENT_TIMESTAMP())");
		sb.append("        ,RETURN_STAFF_ID = ?");
		sb.append("        ,STATUS_ID = ?");
		sb.append("    WHERE");
		sb.append("        RENTAL_NUMBER = ?");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, id);
			ps.setInt(2, statusId);
			ps.setInt(3, rentalNumber);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}		
	}
	
	public boolean isDisposalIdentificationNumber(ArrayList<Integer> itemId, ArrayList<Integer> identificationNumber) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ITEM.ITEM_ID");
		sb.append("    FROM");
		sb.append("        ITEM");
		sb.append("            LEFT OUTER JOIN DISPOSAL_IDENTIFICATION_NUMBER");
		sb.append("                ON ITEM.ITEM_ID = DISPOSAL_IDENTIFICATION_NUMBER.ITEM_ID");
		sb.append("    WHERE");
		sb.append("        ITEM.ITEM_ID = ?");
		sb.append("        AND (");
		sb.append("            DISPOSAL_IDENTIFICATION_NUMBER.IDENTIFICATION_NUMBER = ?");
		sb.append("            OR MAX_IDENTIFICATION_NUMBER < ?");
		sb.append("        )");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ResultSet rs;
			for (int i = 0; i < itemId.size(); i++) {
				ps.setInt(1, itemId.get(i));
				ps.setInt(2, identificationNumber.get(i));
				ps.setInt(3, identificationNumber.get(i));
				rs = ps.executeQuery();
				if (rs.next()) {
					return true;
				}
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return false;
	}
	
	public boolean updateItenIdentificationNumber(int rentalNumber, ArrayList<Integer> itemIdList, ArrayList<Integer> identificationNumberList) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        RENTAL_DETAIL");
		sb.append("    SET");
		sb.append("        IDENTIFICATION_NUMBER = ?");
		sb.append("    WHERE");
		sb.append("        RENTAL_NUMBER = ?");
		sb.append("        AND ITEM_ID = ?");
		sb.append("        AND IDENTIFICATION_NUMBER <=> NULL LIMIT 1");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(2, rentalNumber);
			int count = 0;
			for (int i = 0; i < itemIdList.size(); i++) {
				ps.setInt(1, identificationNumberList.get(i));
				ps.setInt(3, itemIdList.get(i));
				count += ps.executeUpdate();
			}
			
			if (count == itemIdList.size()) {
				return true;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return false;
	}
}