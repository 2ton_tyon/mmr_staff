package jp.staff.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.staff.SQLRuntimeException;
import jp.staff.dto.StaffDTO;

public class StaffDAO {
	
	protected Connection connection;
	
	public StaffDAO(Connection connection){
		this.connection = connection;
	}
	
	public boolean loginCheckMMRStaff(String id, String password) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUNT(*) 'count'");
		sb.append("    FROM");
		sb.append("        mmr_online.staff");
		sb.append("    WHERE");
		sb.append("        staff_ID = ?");
		sb.append("        AND password = password(?)");
		sb.append("        AND delete_flg = '0'");
	

		
		try{
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, id);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				return rs.getInt("COUNT") == 1;
			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}
		
		return false;

	}
	
	public boolean reRegistPassword(String pass, String id) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        STAFF");
		sb.append("    SET");
		sb.append("        PASSWORD = PASSWORD(?)");
		sb.append("    WHERE");
		sb.append("        STAFF_ID = ?");
		
		try{
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, pass);
			ps.setString(2, id);
			ps.executeUpdate();
			return true;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	
	public ArrayList<StaffDTO> selectAll() throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        STAFF_ID");
		sb.append("        ,MANAGER_FLG");
		sb.append("        ,NAME");
		sb.append("    FROM");
		sb.append("        STAFF");
		sb.append("    WHERE");
		sb.append("        DELETE_FLG = 0");


		ArrayList<StaffDTO> list = new ArrayList<StaffDTO>();
		try{
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				StaffDTO staffRowData = new StaffDTO();
				staffRowData.setStaffId(rs.getString("STAFF_ID"));
				staffRowData.setName(rs.getString("NAME"));
				staffRowData.setMgrRights(rs.getInt("MANAGER_FLG"));
				list.add(staffRowData);
			}
			
			return list;
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	public Integer selectByAuth(String id) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        MANAGER_FLG");
		sb.append("    FROM");
		sb.append("        STAFF");
		sb.append("    WHERE");
		sb.append("        STAFF_ID = ?");
		
		Integer auth = null;
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				auth =  rs.getInt("MANAGER_FLG");
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		
		return auth;
	}
	
	public int deleteStaff(String id) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        STAFF");
		sb.append("    SET");
		sb.append("        DELETE_FLG = 1");
		sb.append("    WHERE");
		sb.append("        STAFF_ID = ?");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, id);
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	public int updateStaff(StaffDTO staffDTO) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        STAFF");
		sb.append("    SET");
		sb.append("        STAFF_ID = ?");
		sb.append("        ,MANAGER_FLG = ?");
		sb.append("        ,NAME = ?");
		sb.append("    WHERE");
		sb.append("        STAFF_ID = ?");

		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, staffDTO.getStaffId());
			ps.setInt(2, staffDTO.getMgrRights());
			ps.setString(3, staffDTO.getName());
			ps.setString(4, staffDTO.getStaffId());
			  return ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	public int insertStaff(StaffDTO staffDTO) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        STAFF");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,PASSWORD('MMR_ONLINE')");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,0");
		sb.append("        )");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, staffDTO.getStaffId());
			ps.setInt(2, staffDTO.getMgrRights());
			ps.setString(3, staffDTO.getName());
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	public int resetPassword(String id) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        STAFF");
		sb.append("    SET");
		sb.append("        PASSWORD = PASSWORD('MMR_ONLINE')");
		sb.append("    WHERE");
		sb.append("        STAFF_ID = ?");
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, id);
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
}
