package jp.staff.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.dto.UploadDto;

public class UploadDao {
	
	Connection connection;
	
	public UploadDao (Connection connection) {
		this.connection = connection;
	}

	public int upload(UploadDto dto) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append(" UPDATE");
		sb.append("        item");
		sb.append("    SET");
		sb.append("        image = ?");
		sb.append("  WHERE");
		sb.append("        item_id = ?");

		try (Connection connection = DataSourceManager.getConnection()) {
			
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setBinaryStream(1, dto.getImage());
			ps.setInt(2, dto.getId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

}
