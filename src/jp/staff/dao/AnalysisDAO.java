package jp.staff.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.staff.SQLRuntimeException;
import jp.staff.dto.AnalysisDTO;

public class AnalysisDAO {
	
	private Connection connection;
	
	/**
	 * コネクションをセットする
	 * @param conn
	 */
	public AnalysisDAO(Connection connection) {
		this.connection = connection;
	}
	
	public ArrayList<AnalysisDTO> selectData(String year, int startAge, int endAge) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        SUM(price) AS FEE");
		sb.append("        ,CASE");
		sb.append("            WHEN 1 = month(ORDER_DATETIME) THEN 1");
		sb.append("            WHEN 2 = month(ORDER_DATETIME) THEN 2");
		sb.append("            WHEN 3 = month(ORDER_DATETIME) THEN 3");
		sb.append("            WHEN 4 = month(ORDER_DATETIME) THEN 4");
		sb.append("            WHEN 5 = month(ORDER_DATETIME) THEN 5");
		sb.append("            WHEN 6 = month(ORDER_DATETIME) THEN 6");
		sb.append("            WHEN 7 = month(ORDER_DATETIME) THEN 7");
		sb.append("            WHEN 8 = month(ORDER_DATETIME) THEN 8");
		sb.append("            WHEN 9 = month(ORDER_DATETIME) THEN 9");
		sb.append("            WHEN 10 = month(ORDER_DATETIME) THEN 10");
		sb.append("            WHEN 11 = month(ORDER_DATETIME) THEN 11");
		sb.append("            WHEN 12 = month(ORDER_DATETIME) THEN 12");
		sb.append("        END AS SEARCH_DATE");
		sb.append("        ,CASE");
		sb.append("            WHEN 18 < (");
		sb.append("                (year(curdate()) - year(birthday)) - (");
		sb.append("                        RIGHT (");
		sb.append("                            curdate()");
		sb.append("                            ,5");
		sb.append("                        ) <");
		sb.append("                            RIGHT (");
		sb.append("                                birthday");
		sb.append("                                ,5");
		sb.append("                            )");
		sb.append("                )");
		sb.append("            )");
		sb.append("            AND (");
		sb.append("                (year(curdate()) - year(birthday)) - (");
		sb.append("                        RIGHT (");
		sb.append("                            curdate()");
		sb.append("                            ,5");
		sb.append("                        ) <");
		sb.append("                            RIGHT (");
		sb.append("                                birthday");
		sb.append("                                ,5");
		sb.append("                            )");
		sb.append("                )");
		sb.append("            ) < 29 THEN '18-29'");
		sb.append("            WHEN (");
		sb.append("                (year(curdate()) - year(birthday)) - (");
		sb.append("                        RIGHT (");
		sb.append("                            curdate()");
		sb.append("                            ,5");
		sb.append("                        ) <");
		sb.append("                            RIGHT (");
		sb.append("                                birthday");
		sb.append("                                ,5");
		sb.append("                            )");
		sb.append("                )");
		sb.append("            ) < 39 THEN '30代'");
		sb.append("            WHEN (");
		sb.append("                (year(curdate()) - year(birthday)) - (");
		sb.append("                        RIGHT (");
		sb.append("                            curdate()");
		sb.append("                            ,5");
		sb.append("                        ) <");
		sb.append("                            RIGHT (");
		sb.append("                                birthday");
		sb.append("                                ,5");
		sb.append("                            )");
		sb.append("                )");
		sb.append("            ) < 49 THEN '40代'");
		sb.append("            WHEN (");
		sb.append("                (year(curdate()) - year(birthday)) - (");
		sb.append("                        RIGHT (");
		sb.append("                            curdate()");
		sb.append("                            ,5");
		sb.append("                        ) <");
		sb.append("                            RIGHT (");
		sb.append("                                birthday");
		sb.append("                                ,5");
		sb.append("                            )");
		sb.append("                )");
		sb.append("            ) < 59 THEN '50代'");
		sb.append("            WHEN (");
		sb.append("                (year(curdate()) - year(birthday)) - (");
		sb.append("                        RIGHT (");
		sb.append("                            curdate()");
		sb.append("                            ,5");
		sb.append("                        ) <");
		sb.append("                            RIGHT (");
		sb.append("                                birthday");
		sb.append("                                ,5");
		sb.append("                            )");
		sb.append("                )");
		sb.append("            ) >= 60 THEN '60歳以上'");
		sb.append("        END AS Age");
		sb.append("    FROM");
		sb.append("        rental_detail");
		sb.append("            LEFT JOIN rental");
		sb.append("                ON rental.rental_number = rental_detail.rental_number");
		sb.append("            LEFT JOIN USER");
		sb.append("                ON rental.USER_ID = user.USER_ID");
		sb.append("            LEFT JOIN item");
		sb.append("                ON rental_detail.ITEM_ID = item.ITEM_ID");
		sb.append("            LEFT JOIN coupon");
		sb.append("                ON coupon.coupon_id = rental.USE_COUPON_ID");
		sb.append("    WHERE");
		sb.append("        (year(ORDER_DATETIME)) = ?");
		sb.append("        AND rental.USE_COUPON_ID IS NOT null");
		sb.append("        AND ? < (");
		sb.append("            (year(curdate()) - year(birthday)) - (");
		sb.append("                    RIGHT (");
		sb.append("                        curdate()");
		sb.append("                        ,5");
		sb.append("                    ) <");
		sb.append("                        RIGHT (");
		sb.append("                            birthday");
		sb.append("                            ,5");
		sb.append("                        )");
		sb.append("            )");
		sb.append("        )");
		sb.append("        AND (");
		sb.append("            (year(curdate()) - year(birthday)) - (");
		sb.append("                    RIGHT (");
		sb.append("                        curdate()");
		sb.append("                        ,5");
		sb.append("                    ) <");
		sb.append("                        RIGHT (");
		sb.append("                            birthday");
		sb.append("                            ,5");
		sb.append("                        )");
		sb.append("            )");
		sb.append("        ) < ?");
		sb.append("    GROUP BY");
		sb.append("        search_date");
		sb.append("        ,age");
		sb.append("    ORDER BY");
		sb.append("        search_date");
		
		ArrayList<AnalysisDTO> analysis = new ArrayList<>();
		
		try {
			
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, year);
			ps.setInt(2, startAge);
			ps.setInt(3, endAge);
			
			ResultSet rs = ps.executeQuery();
			int cnt = 1;
			while (rs.next()) {
				while  (cnt != rs.getInt("SEARCH_DATE")) {
					AnalysisDTO analysisDTO = new AnalysisDTO();
					analysisDTO.setAmountSold(0);
					analysisDTO.setMonth(cnt);
					analysis.add(analysisDTO);
					cnt += 1;
				}
				AnalysisDTO analysisDTO2 = new AnalysisDTO();
				analysisDTO2.setAmountSold(rs.getInt("FEE"));
				analysisDTO2.setMonth(rs.getInt("SEARCH_DATE"));
				analysis.add(analysisDTO2);
				cnt += 1;
			}
			
			while (cnt != 13) {
				AnalysisDTO analysisDTO = new AnalysisDTO();
				analysisDTO.setAmountSold(0);
				analysisDTO.setMonth(cnt);
				analysis.add(analysisDTO);
				cnt += 1;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return analysis;
	}
}
