package jp.staff.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.staff.SQLRuntimeException;
import jp.staff.dto.CouponDTO;

public class CouponDAO {
	
	private Connection connection;
		
	/**
	 * コネクションをセットする
	 * @param conn
	 */
	public CouponDAO(Connection connection) {
		this.connection = connection;
	}
	
	public ArrayList<CouponDTO> selectAll() throws SQLRuntimeException {
		// 1 クーポンの内容を取得する
		// 2 クーポンの配布枚数を取得する			
		// 3 クーポンの使用枚数を取得する
		// 4 クーポン利用による割引金額を求める
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUPON_ID");
		sb.append("        ,COUPON_CONTENT");
		sb.append("        ,ACTIVE_FROM");
		sb.append("        ,ACTIVE_UNTIL");
		sb.append("        ,DISCOUNT_RATE");
		sb.append("    FROM");
		sb.append("        COUPON");
		sb.append("    ORDER BY");
		sb.append("        ACTIVE_UNTIL DESC");
		
		StringBuffer sb2 = new StringBuffer();
		sb2.append("SELECT");
		sb2.append("        COUPON_ID");
		sb2.append("        ,COUNT(*) AS CNT");
		sb2.append("    FROM");
		sb2.append("        HAVING_COUPON");
		sb2.append("    WHERE");
		sb2.append("        COUPON_ID = ?");
		sb2.append("    GROUP BY");
		sb2.append("        COUPON_ID");
		
		StringBuffer sb3 = new StringBuffer();
		sb3.append("SELECT");
		sb3.append("        COUNT(*) AS CNT");
		sb3.append("    FROM");
		sb3.append("        HAVING_COUPON");
		sb3.append("    WHERE");
		sb3.append("        USED_FLG = 1");
		sb3.append("        AND COUPON_ID = ?");
		sb3.append("    GROUP BY");
		sb3.append("        COUPON_ID");
		
		StringBuffer sb4 = new StringBuffer();
		sb4.append("SELECT");
		sb4.append("        SUM(ITEM.PRICE) * (COUPON.DISCOUNT_RATE / 100) AS TOTAL");
		sb4.append("    FROM");
		sb4.append("        RENTAL");
		sb4.append("            LEFT JOIN RENTAL_DETAIL");
		sb4.append("                ON RENTAL.RENTAL_NUMBER = RENTAL_DETAIL.RENTAL_NUMBER");
		sb4.append("            LEFT JOIN ITEM");
		sb4.append("                ON RENTAL_DETAIL.ITEM_ID = ITEM.ITEM_ID");
		sb4.append("            LEFT JOIN COUPON");
		sb4.append("                ON COUPON.COUPON_ID = RENTAL.USE_COUPON_ID");
		sb4.append("    WHERE");
		sb4.append("        COUPON.COUPON_ID = ?");
	
		ArrayList<CouponDTO> couponList = new ArrayList<>();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			PreparedStatement ps2 = connection.prepareStatement(sb2.toString());
			PreparedStatement ps3 = connection.prepareStatement(sb3.toString());
			PreparedStatement ps4 = connection.prepareStatement(sb4.toString());
			
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				CouponDTO coupon = new CouponDTO();
				coupon.setId(rs.getInt("COUPON_ID"));
				coupon.setContent(rs.getString("COUPON_CONTENT"));
				coupon.setActiveFrom(rs.getString("ACTIVE_FROM"));
				coupon.setActiveUntil(rs.getString("ACTIVE_UNTIL"));
				coupon.setDiscountRate(rs.getInt("DISCOUNT_RATE"));
				
				ps2.setInt(1, rs.getInt("COUPON_ID"));
				ps3.setInt(1, rs.getInt("COUPON_ID"));
				ps4.setInt(1, rs.getInt("COUPON_ID"));
				
				ResultSet rSet = ps2.executeQuery();
				if (rSet.next()) {
					coupon.setDistributionNumber(rSet.getInt("CNT"));
				}
				
				rSet = ps3.executeQuery();
				if (rSet.next()) {
					coupon.setUsedNumber(rSet.getInt("CNT"));
				}
				
				rSet = ps4.executeQuery();
				if (rSet.next()) {
					coupon.setTotalDiscountAmount(rSet.getInt("TOTAL"));
				}
				
				couponList.add(coupon);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return couponList;
	}
	
	public CouponDTO selectCoupon (int id) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUPON_ID");
		sb.append("        ,COUPON_CONTENT");
		sb.append("        ,ACTIVE_FROM");
		sb.append("        ,ACTIVE_UNTIL");
		sb.append("        ,DISCOUNT_RATE");
		sb.append("    FROM");
		sb.append("        COUPON");
		sb.append("    WHERE");
		sb.append("        COUPON_ID = ?");
		
		CouponDTO couponDTO = new CouponDTO();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				couponDTO.setId(rs.getInt("COUPON_ID"));
				couponDTO.setContent(rs.getString("COUPON_CONTENT"));
				couponDTO.setActiveFrom(rs.getString("ACTIVE_FROM"));
				couponDTO.setActiveUntil(rs.getString("ACTIVE_UNTIL"));
				couponDTO.setDiscountRate(rs.getInt("DISCOUNT_RATE"));
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return couponDTO;
	}
	
	public int updateCoupon(CouponDTO couponDTO) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        COUPON");
		sb.append("    SET");
		sb.append("        COUPON_CONTENT = ?");
		sb.append("        ,ACTIVE_FROM = ?");
		sb.append("        ,ACTIVE_UNTIL = ?");
		sb.append("        ,DISCOUNT_RATE = ?");
		sb.append("    WHERE");
		sb.append("        COUPON_ID = ?");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, couponDTO.getContent());
			ps.setString(2, couponDTO.getActiveFrom());
			ps.setString(3, couponDTO.getActiveUntil());
			ps.setInt(4, couponDTO.getDiscountRate());
			ps.setInt(5, couponDTO.getId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	public int insertCoupon(CouponDTO couponDTO) throws SQLRuntimeException {
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        COUPON(");
		sb.append("            COUPON_CONTENT");
		sb.append("            ,ACTIVE_FROM");
		sb.append("            ,ACTIVE_UNTIL");
		sb.append("            ,DISCOUNT_RATE");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("        )");

		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, couponDTO.getContent());
			ps.setString(2, couponDTO.getActiveFrom());
			ps.setString(3, couponDTO.getActiveUntil());
			ps.setInt(4, couponDTO.getDiscountRate());
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	public ArrayList<CouponDTO> selectCouponDiscountRate(String year) throws SQLRuntimeException {
		
		StringBuffer inputYear = new StringBuffer();
		inputYear.append(year);
		inputYear.append("%");
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        DISCOUNT_RATE");
		sb.append("    FROM");
		sb.append("        COUPON");
		sb.append("    WHERE");
		sb.append("        ACTIVE_FROM LIKE ?");
		sb.append("    ORDER BY");
		sb.append("        ACTIVE_FROM LIMIT 12");
		
		ArrayList<CouponDTO> discountRateList = new ArrayList<>();
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setString(1, inputYear.toString());
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				CouponDTO couponDTO = new CouponDTO();
				couponDTO.setDiscountRate(rs.getInt("DISCOUNT_RATE"));
				discountRateList.add(couponDTO);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return discountRateList;
	}
	
	public int deleteHavingCoupon(int id) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("DELETE");
		sb.append("    FROM");
		sb.append("        HAVING_COUPON");
		sb.append("    WHERE");
		sb.append("        COUPON_ID = ?;");

		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, id);
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	public int deleteCoupon(int id) throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("DELETE");
		sb.append("    FROM");
		sb.append("        COUPON");
		sb.append("    WHERE");
		sb.append("        COUPON_ID = ?");

		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ps.setInt(1, id);
			  return ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	
	public int selectCouponEditArea() throws SQLRuntimeException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUPON_ID");
		sb.append("    FROM");
		sb.append("        COUPON");
		sb.append("    WHERE");
		sb.append("        (");
		sb.append("            CURDATE() + INTERVAL 1 MONTH");
		sb.append("        ) > ACTIVE_FROM");
		sb.append("        AND (");
		sb.append("            CURDATE() + INTERVAL 1 MONTH");
		sb.append("        ) < ACTIVE_UNTIL");
		
		try {
			PreparedStatement ps = connection.prepareStatement(sb.toString());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt("COUPON_ID");
			}
			return 0;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
}


