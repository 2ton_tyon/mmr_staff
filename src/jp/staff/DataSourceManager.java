package jp.staff;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DataSourceManager {
	
	/**
	 * コネクションプールからコネクションを取得する
	 * @return コネクション
	 * @throws SQLException　DB接続エラー
	 */
	public static Connection getConnection() throws SQLException{
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/mysql");
			return  ds.getConnection(); 
		} catch (NamingException | SQLException e ) {
			throw new SQLException();
		}
	}
}