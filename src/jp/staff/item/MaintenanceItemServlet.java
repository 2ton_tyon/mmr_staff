package jp.staff.item;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.dao.CategoryDAO;
import jp.staff.dao.ItemDAO;
import jp.staff.dto.ItemDTO;

/**
 * Servlet implementation class MaintenanceItemServlet
 */
@WebServlet("/MaintenanceItem")
public class MaintenanceItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null || (Integer) session.getAttribute("auth") != 1) {
			response.sendRedirect("login");
			return;
		}
		
		try (Connection connection = DataSourceManager.getConnection()) {
			
			// カテゴリー、ジャンルの取得
			CategoryDAO categoryDAO = new CategoryDAO(connection);
			ArrayList<String> categoryList = categoryDAO.selectCategory();
			HashMap<String, ArrayList<String>> list = new HashMap<>();
			for(String category : categoryList) {
				list.put(category, categoryDAO.selectGenreByCategory(category));
			}
			request.setAttribute("category", categoryList);
			request.setAttribute("allGenreList", list);
			
			// ItemDAOから商品のID、新旧区分、カテゴリ、タイトル、アーティスト,値段の取得
			ItemDAO itemDAO = new ItemDAO(connection);
			ArrayList<ItemDTO> itemList = itemDAO.selectALL();
			request.setAttribute("itemList", itemList);
			
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
			return;
		}
		request.getRequestDispatcher("WEB-INF/jsp/itemMaintenance.jsp").forward(request, response);
	}
}
