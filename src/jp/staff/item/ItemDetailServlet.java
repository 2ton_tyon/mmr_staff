package jp.staff.item;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.dao.ItemDAO;
import jp.staff.dto.CategoryDTO;
import jp.staff.dto.GenreDTO;
import jp.staff.dto.ItemDTO;

/**
 * Servlet implementation class ItemDetailServlet
 */
@WebServlet("/ItemDetail")
public class ItemDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 * 既存の商品情報を参照するときに呼ばれる
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null || (Integer)session.getAttribute("auth") != 1) {
			response.sendRedirect("login");
		}
		String itemId = request.getParameter("itemId");
		if (itemId == null || "".equals(itemId)) {
			response.sendRedirect("MaintenanceItem");
		}
		int id = 0;
		try {
			id = Integer.parseInt(itemId);
		} catch (NumberFormatException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystenError.jsp").forward(request, response);
		}
		try (Connection connection = DataSourceManager.getConnection()) {
			// カテゴリー、ジャンルの取得
			ItemDAO itemDAO = new ItemDAO(connection);
			ArrayList<CategoryDTO> categoryList = itemDAO.selectCategory();
			ArrayList<GenreDTO> genreList  =itemDAO.selectGenre();
			request.setAttribute("categoryList", categoryList);
			request.setAttribute("genreList", genreList);
			ItemDTO itemDTO = itemDAO.selectItemDetail(id);
			request.setAttribute("item", itemDTO);
			request.getRequestDispatcher("WEB-INF/jsp/itemDetail.jsp").forward(request, response);
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
		}
	}
}
