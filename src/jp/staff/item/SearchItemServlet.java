package jp.staff.item;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.dao.CategoryDAO;
import jp.staff.dao.ItemDAO;
import jp.staff.dto.ItemDTO;

/**
 * Servlet implementation class SearchItemServlet
 */
@WebServlet("/search")
public class SearchItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Integer auth = (Integer) session.getAttribute("auth");
		
		if (session.getAttribute("id") == null || auth != 1) {
			response.sendRedirect("login");
			return;
		}
		
		String searchCategory = request.getParameter("category");
		String searchGenre = request.getParameter("genre");
		String searchWord = request.getParameter("search");
		try (Connection connection = DataSourceManager.getConnection()) {
			// カテゴリー、ジャンルの取得
			CategoryDAO categoryDAO = new CategoryDAO(connection);
			ArrayList<String> categoryList = categoryDAO.selectCategory();
			HashMap<String, ArrayList<String>> list = new HashMap<>();
			for(String category : categoryList) {
				list.put(category, categoryDAO.selectGenreByCategory(category));
			}
			ItemDAO itemDAO = new ItemDAO(connection);
			ArrayList<ItemDTO> itemList = itemDAO.selectSearchItem(searchCategory, searchGenre, searchWord);
			request.setAttribute("category", categoryList);
			request.setAttribute("allGenreList", list);
			request.setAttribute("itemList", itemList);
			request.getRequestDispatcher("WEB-INF/jsp/itemMaintenance.jsp").forward(request, response);
			
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
		}
	}
}
