package jp.staff.item;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.staff.DataSourceManager;
import jp.staff.ParseData;
import jp.staff.SQLRuntimeException;
import jp.staff.Validate;
import jp.staff.dao.ItemDAO;
import jp.staff.dto.ItemDTO;

/**
 * Servlet implementation class UpdateItemServlet
 */
@WebServlet("/UpdateItem")
public class UpdateItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null) {
			response.sendRedirect("login");
		} else {
			response.sendRedirect("rental");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null || (Integer) session.getAttribute("auth") != 1) {
			response.sendRedirect("login");
			return;
		}
		
		request.setCharacterEncoding("utf-8");
		String id = request.getParameter("itemId");
		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String[] genre = request.getParameterValues("genre");
		String newAndOld = request.getParameter("newAndOld");
		String artist = request.getParameter("artist");
		String price = request.getParameter("price");
		String recommend = request.getParameter("recommend");
		String remarks = request.getParameter("remarks");
		String identificationNumber = request.getParameter("identification");
		String[] disposalList = request.getParameterValues("disposal");
		
		// 備考欄を除いた項目は必須項目なのでnullまたは空文字でないことを確認。
		//　数値のものはIntに変換できるかも確認
		if (Validate.isStrParseInt(id) && !Validate.isEmpty(title) && !Validate.isEmpty(category) && Validate.isStrParseIntList(genre) 
				&& Validate.isStrParseInt(newAndOld) && !Validate.isEmpty(artist) && Validate.isStrParseInt(price) 
				&& Validate.isStrParseInt(recommend) && Validate.isStrParseInt(identificationNumber)){
			
			if (disposalList != null && Validate.isStrParseIntList(disposalList)) {
				session.setAttribute("errorMsg", "更新に失敗しました。");
				response.sendRedirect("ItemDetail?itemId="+id);
				return;
			}
			
			try (Connection connection = DataSourceManager.getConnection()) {
				ItemDTO itemDTO = new ItemDTO();
				itemDTO.setItem_id(Integer.parseInt(id));
				itemDTO.setItemName(title);
				itemDTO.setCategoryId(Integer.parseInt(category));
				itemDTO.setGenreId(ParseData.parseStringListToIntegerArray(genre));
				itemDTO.setNewAndOldId(Integer.parseInt(newAndOld));
				itemDTO.setArtist(artist);
				itemDTO.setPrice(Integer.parseInt(price));
				itemDTO.setRecommendedFlag(Integer.parseInt(recommend));
				itemDTO.setRemarks(remarks);
				itemDTO.setMaxIdentificationNumber(Integer.parseInt(identificationNumber));
				itemDTO.setDisposalIdentificationNumber(ParseData.parseStringListToIntegerArray(disposalList));
				
				ItemDAO itemDAO = new ItemDAO(connection);
				
				connection.setAutoCommit(false);
				
				// 1 ITEMの情報を更新する
				// 2　現在のジャンルを削除する
				// 3  新しいジャンルを挿入する
				// 4 現在の廃棄番号リストを削除する
				// 5 新規の廃棄番号リストを挿入する
				// 6 commit
				if(itemDAO.updateItem(itemDTO) != 1 || !itemDAO.deleteItemGenre(Integer.parseInt(id)) || itemDAO.insertItemGenre(itemDTO) != itemDTO.getGenreId().size() || 
						!itemDAO.deleteDisposalIdentificationNumber(Integer.parseInt(id)) || 
						!itemDAO.insertDisposalIdentificationNumber(itemDTO) ){
					session.setAttribute("errorMsg", "更新に失敗しました。");
					response.sendRedirect("ItemDetail?itemId="+id);
					connection.rollback();
					return;
				};
				connection.commit();
				session.setAttribute("msg", "更新が完了しました");
				response.sendRedirect("ItemDetail?itemId="+id);
				
			} catch (SQLException | SQLRuntimeException e) {
				request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
			}
		} else {
			session.setAttribute("errorMsg", "不適切な値が入力されました。入力しなおしてください。");
			response.sendRedirect("ItemDetail?itemId="+id);
		}
	}
}
