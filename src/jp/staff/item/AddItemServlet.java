package jp.staff.item;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.staff.DataSourceManager;
import jp.staff.ParseData;
import jp.staff.SQLRuntimeException;
import jp.staff.Validate;
import jp.staff.dao.ItemDAO;
import jp.staff.dto.CategoryDTO;
import jp.staff.dto.GenreDTO;
import jp.staff.dto.ItemDTO;

/**
 * Servlet implementation class AddItemServlet
 */
@WebServlet("/Add")
public class AddItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null || (Integer)session.getAttribute("auth") != 1) {
			response.sendRedirect("login");
			return;
		}
		try (Connection connection = DataSourceManager.getConnection()) {
			// カテゴリー、ジャンルの取得
			ItemDAO itemDAO = new ItemDAO(connection);
			ArrayList<CategoryDTO> categoryList = itemDAO.selectCategory();
			ArrayList<GenreDTO> genreList  =itemDAO.selectGenre();
			request.setAttribute("categoryList", categoryList);
			request.setAttribute("genreList", genreList);
			request.getRequestDispatcher("WEB-INF/jsp/addItem.jsp").forward(request, response);
		} catch (SQLException | SQLRuntimeException e) {
			request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null || (Integer) session.getAttribute("auth") != 1) {
			response.sendRedirect("login");
			return;
		}
		
		request.setCharacterEncoding("utf-8");
		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String[] genre = request.getParameterValues("genre");
		String newAndOld = request.getParameter("newAndOld");
		String artist = request.getParameter("artist");
		String price = request.getParameter("price");
		String recommend = request.getParameter("recommend");
		String remarks = request.getParameter("remarks");
		String identificationNumber = request.getParameter("identification");
		
		// 備考欄を除いた項目は必須項目なのでnullまたは空文字でないことを確認。
		//　数値のものはIntに変換できるかも確認
		if (!Validate.isEmpty(title) && !Validate.isEmpty(category) && Validate.isStrParseIntList(genre) 
				&& Validate.isStrParseInt(newAndOld) && !Validate.isEmpty(artist) && Validate.isStrParseInt(price) 
				&& Validate.isStrParseInt(recommend) && Validate.isStrParseInt(identificationNumber)){
			
			try (Connection connection = DataSourceManager.getConnection()) {
				ItemDTO itemDTO = new ItemDTO();
				itemDTO.setItemName(title);
				itemDTO.setCategoryId(Integer.parseInt(category));
				itemDTO.setGenreId(ParseData.parseStringListToIntegerArray(genre));
				itemDTO.setNewAndOldId(Integer.parseInt(newAndOld));
				itemDTO.setArtist(artist);
				itemDTO.setPrice(Integer.parseInt(price));
				itemDTO.setRecommendedFlag(Integer.parseInt(recommend));
				itemDTO.setRemarks(remarks);
				itemDTO.setMaxIdentificationNumber(Integer.parseInt(identificationNumber));
				
				ItemDAO itemDAO = new ItemDAO(connection);
				
				connection.setAutoCommit(false);
				
				// 1 ITEMの情報を登録する
				
				Integer itemId = itemDAO.insertItem(itemDTO);
				
				if (itemId == 0) {
					// 追加失敗
					session.setAttribute("errorMsg", "更新に失敗しました。");
					doGet(request, response);
					connection.rollback();
				}
				itemDTO.setItem_id(itemId);
				// 2  新しいジャンルを挿入する
				if(itemDAO.insertItemGenre(itemDTO) != itemDTO.getGenreId().size()){
					session.setAttribute("errorMsg", "更新に失敗しました。");
					doGet(request, response);
					connection.rollback();
					return;
				};
				// 3 commit
				connection.commit();
				session.setAttribute("msg", "更新が完了しました");
				response.sendRedirect("ItemDetail?itemId="+itemId);
				
			} catch (SQLException | SQLRuntimeException e) {
				request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
			}	
			
		} else {
			session.setAttribute("errorMsg", "更新に失敗しました。");
			doGet(request, response);
			return;
		}
	}

}
