package jp.staff.item;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import jp.staff.DataSourceManager;
import jp.staff.SQLRuntimeException;
import jp.staff.dao.UploadDao;
import jp.staff.dto.UploadDto;


/**
 * Servlet implementation class FileUploadServlet
 */
@WebServlet("/fileUpload")
@MultipartConfig(location="")
public class FileUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		try (Connection connection = DataSourceManager.getConnection()) {
			UploadDto dto = new UploadDto();
			for(Part p :request.getParts()){
				if ("file".equals(p.getName())) {
					dto.setImage(p.getInputStream());
				}
			}
			dto.setId(Integer.parseInt(request.getParameter("id")));
			UploadDao dao = new UploadDao(connection);
			if (1 == dao.upload(dto)) {
				response.sendRedirect("ItemDetail?itemId="+dto.getId());
			} else {
				request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
			}
			
		} catch (SQLException | SQLRuntimeException e) {
			e.printStackTrace();
			request.getRequestDispatcher("WEB-INF/jsp/SystemError.jsp").forward(request, response);
		}
	}

}
